package one.business;

import fzs.wechat.binder.TransceiverCenter;
import fzs.wechat.shared.wechat.models.ServiceConfig;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Vertx;

/**
 * Created by WEI on 2017/3/4.
 */
public class MainVerticle extends AbstractVerticle {
    @Override
    public void start(Future<Void> startFuture) throws Exception {
        ServiceConfig wechatConfig = new ServiceConfig(config());
        if (wechatConfig == null) {
            startFuture.fail("请检查微信配置");
            return;
        }
        Future<Vertx> deployFuture = Future.future();
        deployFuture.setHandler((AsyncResult<Vertx> ar) -> {
            if (ar.succeeded()) {
                TencentWeChatConfigurator.config();
                new RouterConfigurator().config(ar.result(), 8002);

                startFuture.complete();
            } else {
                startFuture.fail(ar.cause());
            }
        });

        TransceiverCenter.deployAsCluster(wechatConfig, deployFuture);
    }
}
