package one.business.modules.store;

import fzs.wechat.shared.module.ModulesAbstract;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Hello on 2017/4/10.
 */
public class ProductsModule implements ModulesAbstract {
    @Override
    public Map<String, Handler<Message<JsonObject>>> getEvents() {
        Map<String, Handler<Message<JsonObject>>> config = new HashMap<>();

        config.put("store.products.get", this::getProducts);

        return config;
    }

    public void getProducts(Message<JsonObject> message) {
        message.reply(new JsonObject()
            .put("success", true)
            .put("message", true)
            .put("datas", new JsonArray()
                    .add(new JsonObject().put("title","测试商品1").put("price","￥8.9"))
                    .add(new JsonObject().put("title","测试商品2").put("price","￥5.9"))
                )
        );
    }
}
