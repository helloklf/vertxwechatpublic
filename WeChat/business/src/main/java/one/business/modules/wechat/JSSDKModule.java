package one.business.modules.wechat;


import fzs.wechat.binder.TransceiverCenter;
import fzs.wechat.shared.module.ModulesAbstract;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Hello on 2017/4/5.
 */
public class JSSDKModule implements ModulesAbstract {

    @Override
    public Map<String, Handler<Message<JsonObject>>> getEvents() {
        HashMap<String, Handler<Message<JsonObject>>> registerInos = new HashMap<>();

        registerInos.put("wechat.jssdk.wxconfig.get", this::getWxConfig);

        return registerInos;
    }

    /**
     * 响应jssdk获取初始化信息请求
     *
     * @param messageHandler
     */
    public void getWxConfig(Message<JsonObject> messageHandler) {
        //获取请求来源地址
        //TODO:有些时候http请求的此参数为空，或者不是真实值，需要使用其它方案替代
        final String refererUrl = messageHandler.headers().get("Referer");

        TransceiverCenter.getInstance().createJSSDKSignature(refererUrl, sign -> {
            JsonObject wxconfig = new JsonObject();
            wxconfig
                .put("debug", "true")
                .put("appId", "wx88ee9996c5695a7d")
                .put("timestamp", sign.getTimestamp())
                .put("nonceStr", sign.getNoncestr())
                .put("signature", sign.getSignature())
                .put("jsApiList", new JsonArray()
                    .add("onMenuShareTimeline")
                    .add("onMenuShareAppMessage")
                    .add("onMenuShareQQ")
                    .add("onMenuShareWeibo")
                    .add("onMenuShareQZone")
                    .add("startRecord")
                    .add("stopRecord")
                    .add("onVoiceRecordEnd")
                    .add("playVoice")
                    .add("pauseVoice")
                    .add("stopVoice")
                    .add("onVoicePlayEnd")
                    .add("uploadVoice")
                    .add("downloadVoice")
                    .add("chooseImage")
                    .add("previewImage")
                    .add("uploadImage")
                    .add("downloadImage")
                    .add("translateVoice")
                    .add("getNetworkType")
                    .add("openLocation")
                    .add("getLocation")
                    .add("hideOptionMenu")
                    .add("showOptionMenu")
                    .add("hideMenuItems")
                    .add("showMenuItems")
                    .add("hideAllNonBaseMenuItem")
                    .add("showAllNonBaseMenuItem")
                    .add("closeWindow")
                    .add("scanQRCode")
                    .add("chooseWXPay")
                    .add("openProductSpecificView")
                    .add("addCard")
                    .add("chooseCard")
                    .add("openCard")
                );

            messageHandler.reply(wxconfig);
        });
    }
}
