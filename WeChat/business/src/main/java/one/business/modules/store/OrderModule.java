package one.business.modules.store;

import fzs.wechat.shared.module.ModulesAbstract;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Hello on 2017/4/10.
 */
public class OrderModule implements ModulesAbstract {
    @Override
    public Map<String, Handler<Message<JsonObject>>> getEvents() {
        Map<String,Handler<Message<JsonObject>>> config = new HashMap<>();

        config.put("store.order.get",this::getOrders);

        return config;
    }

    public void getOrders(Message<JsonObject> message){

    }
}
