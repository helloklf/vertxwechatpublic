package one.business;

import fzs.wechat.binder.TransceiverCenter;
import fzs.wechat.binder.listener.EventListener;
import fzs.wechat.binder.listener.MessageListener;
import fzs.wechat.binder.models.menu.MenuTree;
import fzs.wechat.binder.models.message.ArticlesMessage;
import fzs.wechat.binder.models.response.ArticleResponse;
import fzs.wechat.binder.models.response.TextResponse;
import fzs.wechat.binder.models.response.VideoResponse;
import fzs.wechat.binder.modules.WechatEventReciveModule;
import fzs.wechat.binder.modules.WechatMessageReciveModule;
import fzs.wechat.binder.request.MenuManagement;
import fzs.wechat.binder.request.MessageSender;
import fzs.wechat.shared.module.ModulesManager;
import io.vertx.core.Future;
import one.business.modules.wechat.JSSDKModule;

/**
 * Created by Hello on 2017/4/6.
 */
public class TencentWeChatConfigurator {

    //消息和事件订阅
    public static void config() {
        MessageListener messageListener = new MessageListener() {
            @Override
            public void onTextRecived(String fromUser, String messageContent, Future answer) {
                if (messageContent.equals("创建菜单")) {
                    MenuManagement menuManagement = new MenuManagement();
                    MenuTree menuTree = new MenuTree();
                    menuTree
                        .addEventMenu("试试", "event_1")
                        .addViewMenu("JSSDK", "http://helloklf.iok.la:13168/index.html#begin");
                    Future future = Future.future().setHandler(result -> {
                        answer.complete(new TextResponse().setContent("菜单已创建，你可能需要稍后才能看到效果！"));
                    });
                    menuManagement.setCustomerMenus(menuTree, future);
                } else if (messageContent.equals("获取菜单")) {
                    MenuManagement menuManagement = new MenuManagement();
                    menuManagement.getCustomerMenus(Future.<MenuTree>future().setHandler(result -> {
                        answer.complete(new TextResponse().setContent(result.result().getMenus().encode()));
                    }));
                } else
                    answer.complete(new TextResponse().setContent("听见你说 --> \r\n" + messageContent));
            }

            @Override
            public void onImageRecived(String fromUser, String picUrl, String mediaID, Future answer) {
                answer.complete(new ArticleResponse().addArticle( "标题 爱奇艺","说明 这是个爱奇艺的LOGO","http://www.qiyipic.com/common/fix/site/iqiyi-logo105x50.png","http://www.iqiyi.com/" ));
                new MessageSender().sendMessage(new ArticlesMessage(fromUser).addArticle( "标题 爱奇艺","说明 这是个爱奇艺的LOGO","http://www.qiyipic.com/common/fix/site/iqiyi-logo105x50.png","http://www.iqiyi.com/" ),Future.future());
            }

            @Override
            public void onVoiceRecived(String fromUser, String format, String mediaID, String recognition, Future answer) {
            }

            @Override
            public void onVideoRecived(String fromUser, String mediaID, String thumbMediaId, Future answer) {
                answer.complete(new VideoResponse()
                    .setVidio("I0lpExBzUTjFz9GWKrAXxLOXKwGedwoy3G6w7jSw3ow", "点开看看", "推荐一个视频给你，好像很好看的"));
            }

            @Override
            public void onShortVideoRecived(String fromUser, String mediaID, String thumbMediaId, Future answer) {
                //此事件不知道什么时候触发
            }

            @Override
            public void onLocationRecived(String fromUser, double lat, double lon, int scale, String label, Future answer) {
                answer.complete(new TextResponse().setContent("听见你说 --> \r\n" + label));
            }

            @Override
            public void onLinkRecived(String fromUser, String title, String url, String desc, Future answer) {
            }
        };
        EventListener eventListener = new EventListener() {
            @Override
            public void onSubscribe(String fromUser, Future answer) {
                answer.complete(new TextResponse().setContent("感谢订阅！"));
            }

            @Override
            public void onQrScanAfterSubscribe(String fromUser, String enterKey, String ticket) {
            }

            @Override
            public void onUnSubscribe(String fromUser) {
            }

            @Override
            public void onQrScan(String fromUser, String enterKey, String ticket) {
            }

            @Override
            public void onLocation(String fromUser, Double Latitude, Double Longitude, Double Precision) {
            }

            @Override
            public void onMenuClick(String fromUser, String eventKey, Future answer) {
                answer.complete(new TextResponse().setContent("你点了菜单！"));
            }

            @Override
            public void onMenuOpenView(String fromUser, String eventKey) {
                //用户点击菜单并跳转到页面
            }
        };

        ModulesManager modulesManager = TransceiverCenter.getInstance().getModulesManager();
        try {
            modulesManager.registerModule(new JSSDKModule());
            modulesManager.registerModule(new WechatMessageReciveModule(messageListener));
            modulesManager.registerModule(new WechatEventReciveModule(eventListener));
        } catch (Exception ex) {

        }
    }

}
