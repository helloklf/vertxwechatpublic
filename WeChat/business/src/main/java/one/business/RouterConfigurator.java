package one.business;

import fzs.wechat.binder.TransceiverCenter;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;

/**
 * Created by Hello on 2017/4/5.
 */
public class RouterConfigurator {
    public void config(Vertx vertx, int port) {
        HttpServer httpServer = vertx.createHttpServer();

        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        //假设的一个场景，将功能接口请求统一发送到 localhost:port/wechat/下
        router.route("/wechat/*").handler(this::forward);
        router.route("/store/*").handler(this::forward);
        router.route("/*").handler(StaticHandler.create("Html"));

        vertx.createHttpServer().requestHandler(router::accept).listen(port);
    }

    /**
     * 一个消息转发机制：将收到的请求以事件的形式抛出
     *
     * @param routingContext
     */
    private void forward(RoutingContext routingContext) {
        HttpServerRequest request = routingContext.request();
        String host = request.getHeader("host");

        DeliveryOptions deliveryOptions = new DeliveryOptions() {{
            setSendTimeout(15000);
        }};
        //设置消息标头
        deliveryOptions.setHeaders(routingContext.request().headers());
        JsonObject jsonObject = null;
        try {
            jsonObject = routingContext.getBodyAsJson();
        } catch (Exception ex) {
            jsonObject = new JsonObject();
        }

        Future<JsonObject> future = Future.future();
        future.setHandler(r -> {
            if (r.succeeded())
                routingContext.response()
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(r.result().encode());
            else
                routingContext.fail(500);
        });

        //触发事件
        TransceiverCenter.getInstance().triggerEvent(getEventName(request), jsonObject, deliveryOptions, future);
    }

    /**
     * 将请求转换为事件地址
     *
     * @param request
     * @return
     */
    private String getEventName(HttpServerRequest request) {
        String path = request.path().replace("/", ".");
        String eventName = (path + ((!path.endsWith(".")) ? "." : "") + request.method()).toLowerCase();
        return eventName.startsWith(".") ? eventName.substring(1) : eventName;
    }
}
