package fzs.wechat.shared.module;

import fzs.wechat.shared.EventBusHelper;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 功能模块管理
 * Created by Hello on 2017/3/31.
 */
public class ModulesManager {
    private String eventPrefix;
    private EventBus eventBus;
    private HashMap<String, ArrayList<MessageConsumer>> messageConsumers;
    private ArrayList<String> registedEvents = new ArrayList<>();

    public ModulesManager(String eventPrefix, EventBus eventBus) {
        this.eventPrefix = eventPrefix;
        this.eventBus = eventBus;
    }

    /**
     * 注册功能模块
     *
     * @param moduleInfo
     * @return
     */
    public synchronized ModulesManager registerModule(ModulesAbstract moduleInfo) throws Exception {
        if (messageConsumers == null)
            messageConsumers = new HashMap<>();

        if (moduleInfo != null && eventBus != null && eventPrefix != null) {
            String moduleName = moduleInfo.getClass().getName();
            if (messageConsumers.containsKey(moduleName))
                return this;

            Map<String, Handler<Message<JsonObject>>> configs = moduleInfo.getEvents();

            for (String key : configs.keySet()) {
                if (registedEvents.contains(key))
                    throw new Exception("模块中包含的事件已经在其它模块中注册，重复事件：" + key);
            }

            ArrayList<MessageConsumer> consumers = new ArrayList<>();
            configs.forEach((event, handler) -> {
                registedEvents.add(event);
                String address = eventAddressFormate(event);
                consumers.add(eventBus.consumer(address, handler));
            });
            messageConsumers.put(moduleName, consumers);
        }

        return this;
    }

    /**
     * 移除模块
     *
     * @param moduleInfo
     * @return
     */
    public synchronized ModulesManager unregisterModule(ModulesAbstract moduleInfo) {
        String moduleName = moduleInfo.getClass().getName();
        if (messageConsumers.containsKey(moduleName)) {
            messageConsumers.get(moduleName).forEach(consumer -> {
                consumer.unregister();
            });
            messageConsumers.remove(moduleName);
        }

        return this;
    }

    /**
     * 注册事件监听器：此类消息不需要回复，同一个事件地址可以添加多个事件响应
     *
     * @param eventAddress 事件地址
     * @param eventHandler 事件响应
     * @return
     */
    public synchronized MessageConsumer registerEventListener(String eventAddress, Handler<Message<JsonObject>> eventHandler) {
        return eventBus.consumer(eventAddressFormate(eventAddress), eventHandler);
    }


    /**
     * 解除事件监听
     *
     * @param messageConsumer
     */
    public synchronized void registerEventListener(MessageConsumer messageConsumer) {
        messageConsumer.unregister();
    }


    private String eventAddressFormate(String eventName) {
        return EventBusHelper.formatEventAddress(this.eventPrefix,eventName);
    }
}
