package fzs.wechat.shared.wechat;

/**
 * Created by Hello on 2017/4/6.
 */
public enum MessageType {
    text,
    music,
    image,
    voice,
    video,
    news
}
