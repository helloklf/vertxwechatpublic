package fzs.wechat.shared.wechat.models;

import io.vertx.core.json.JsonObject;

import java.util.ArrayList;

/**
 * 提供对配置文件Json的读取和解析，简化并规范配置文件的读取操作
 * Created by Hello on 2017/3/17.
 */
public class ServiceConfig {
    JsonObject config;

    public ServiceConfig(JsonObject jsonObject) {
        this.config = jsonObject;
    }

    public JsonObject getJsonObject() {
        return config;
    }

    /**
     * @return
     */
    public WechatMicroServiceConfig getMicroServiceConfig() {
        return new WechatMicroServiceConfig(config.getJsonObject("wechat"));
    }

    /**
     * @return
     */
    public ApiProxy getApiProxy() {
        return new ApiProxy(config.getJsonObject("apiproxy"));
    }

    /**
     * @return
     */
    public ArrayList<WeChatApiInfo> getWechatApi() {
        ArrayList<WeChatApiInfo> apiInfos = new ArrayList<>();
        config.getJsonArray("wechatapi").forEach(item -> apiInfos.add(new WeChatApiInfo((JsonObject) item)));

        return apiInfos;
    }
}
