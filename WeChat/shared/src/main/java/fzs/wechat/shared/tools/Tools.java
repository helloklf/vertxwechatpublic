package fzs.wechat.shared.tools;

import io.vertx.core.json.JsonObject;
import org.json.JSONObject;
import org.json.XML;

import java.io.*;
import java.net.URLEncoder;
import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Map;

/**
 * Created by Hello on 2017/3/16.
 */
public class Tools {
    /**
     * 适用于微信JSSDK签名算法的字符串拼接
     *
     * @param jsapi_ticket
     * @param noncestr     随机字符串，可以是guid
     * @param timestamp    时间戳（秒）
     * @param url          页面URL，不包括#及以后的内容
     * @return 签名结果
     */
    public static String wechatSignature(String jsapi_ticket, String noncestr, String timestamp, String url) throws DigestException {
        //jsapi_ticket=&noncestr=&timestamp=&url=
        String decrypt = String.format("jsapi_ticket=%s&noncestr=%s&timestamp=%s&url=%s", jsapi_ticket, noncestr, timestamp, url);
        try {
            //指定sha1算法
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.update(decrypt.getBytes());
            //获取字节数组
            byte messageDigest[] = digest.digest();
            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            // 字节数组转换为 十六进制 数
            for (int i = 0; i < messageDigest.length; i++) {
                String shaHex = Integer.toHexString(messageDigest[i] & 0xFF);
                if (shaHex.length() < 2) {
                    hexString.append(0);
                }
                hexString.append(shaHex);
            }
            return hexString.toString().toUpperCase();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new DigestException("签名错误！");
        }
    }

    public static byte[] readFileToBytes(String filePath) {
        File f = new File(filePath);
        if (!f.exists()) {
            return new byte[0];
        }
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream((int) f.length());
            BufferedInputStream in = null;

            in = new BufferedInputStream(new FileInputStream(f));
            int buf_size = 1024;
            byte[] buffer = new byte[buf_size];
            int len = 0;
            while (-1 != (len = in.read(buffer, 0, buf_size))) {
                bos.write(buffer, 0, len);
            }
            try {
                in.close();
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return bos.toByteArray();
        } catch (Exception ex) {

        }
        return new byte[0];
    }

    /**
     * @param bytes
     * @return
     */
    public static byte[] base64Decode(final byte[] bytes) {
        return Base64.getDecoder().decode(bytes);
    }

    public static byte[] base64Decode(final String str) {
        return Base64.getDecoder().decode(str);
    }

    /**
     * @param url
     * @param params
     * @return
     */
    public static String urlFormate(String url, JsonObject params) {
        StringBuilder stringBuilder = new StringBuilder(url);
        if (!url.contains("?"))
            stringBuilder.append('?');

        if (!url.endsWith("&") && !url.endsWith("?"))
            stringBuilder.append('&');

        params.forEach(item -> {
            Object value = item.getValue();
            if ((!(value instanceof byte[]) && (value != null && value.toString().length() < 4096))) {
                try {
                    stringBuilder.append(item.getKey());
                    stringBuilder.append('=');
                    stringBuilder.append(value == null ? "" : (URLEncoder.encode(value.toString(), "utf-8")));
                    stringBuilder.append('&');
                } catch (Exception ex) {
                }
            }
        });
        return stringBuilder.toString();
    }

    /**
     * 二进制数据编码为BASE64字符串
     *
     * @param bytes
     * @return
     * @throws Exception
     */
    public static String encode(final byte[] bytes) {
        return new String(Base64.getEncoder().encode(bytes));
    }

    /**
     * XML转Json
     *
     * @param xml
     * @return
     */
    public static JsonObject parseXml(String xml) {
        JSONObject orgJsonObject = XML.toJSONObject(xml).getJSONObject("xml");
        JsonObject jsonObject = new JsonObject();
        for (String key :
            orgJsonObject.keySet()) {
            //将XML的key规范为驼峰字
            String firstChar = key.substring(0, 1);
            String camelKey = key.replace(firstChar, firstChar.toLowerCase());
            jsonObject.put(camelKey, orgJsonObject.get(key));
        }
        return jsonObject;
    }


    public static String encodeXml(JsonObject jsonObject) {
        StringBuffer sb = new StringBuffer();
        sb.append("<xml>");
        for (Map.Entry<String, Object> entry : jsonObject) {
            String key = entry.getKey();
            Object value = entry.getValue();
            //将json的key由驼峰字转为首字母也大写
            String firstChar = key.substring(0, 1);
            String upperKey = key.replace(firstChar, firstChar.toUpperCase());
            sb.append("<").append(upperKey).append(">");
            sb.append(String.valueOf(value));
            sb.append("</").append(upperKey).append(">");
        }
        sb.append("</xml>");
        return sb.toString();
    }
}
