package fzs.wechat.shared.tools;

import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Hello on 2017/4/6.
 */
public class HttpUploadFile {
    /**
     * 上传文件
     *
     * @param requestUrl
     * @param fileName    文件名称
     * @param file        文件字节
     * @param description 文件描述信息，微信要求上传视频时提供此内容，其它文件可传null { "title":VIDEO_TITLE, "introduction":INTRODUCTION  }
     * @param future
     */
    public void uploadFile(String requestUrl, String fileName, byte[] file, JsonObject description, Future<JsonObject> future) {
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setUseCaches(false);
            con.setConnectTimeout(5000);
            con.setReadTimeout(30000);
            con.setRequestProperty("Cache-Control", "no-cache");

            // 设置请求头信息
            con.setRequestProperty("Connection", "Keep-Alive");
            con.setRequestProperty("Charset", "UTF-8");
            // 设置边界
            String BOUNDARY = "----------" + System.currentTimeMillis();
            con.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
            byte[] formBegin = ("--" + BOUNDARY + "\r\n").getBytes("utf-8");
            byte[] formEnd = ("\r\n--" + BOUNDARY + "--\r\n").getBytes("utf-8");// 定义最后数据分隔线


            OutputStream requestOutput = new DataOutputStream(con.getOutputStream());

            //写入文件
            StringBuilder fileFormInfo = new StringBuilder();
            fileFormInfo.append("Content-Disposition: form-data;name=\"media\";filename=\"" + fileName + "\";filelength=\"" + file.length + "\"");
            fileFormInfo.append("\r\n");
            fileFormInfo.append("Content-Type:application/octet-stream");
            fileFormInfo.append("\r\n");
            fileFormInfo.append("\r\n");
            requestOutput.write(formBegin);
            requestOutput.write(fileFormInfo.toString().getBytes("utf-8"));
            requestOutput.write(file, 0, file.length);
            requestOutput.write(formEnd);

            //写入描述信息
            if (description != null && description.size() > 0) {
                requestOutput.write(formBegin);
                requestOutput.write("Content-Disposition: form-data; name=\"description\";\r\n\r\n".getBytes("utf-8"));
                requestOutput.write(description.toString().getBytes("utf-8"));
                requestOutput.write(formEnd);
            }
            requestOutput.flush();
            requestOutput.close();
            int code = con.getResponseCode();
            System.out.print(code);

            StringBuffer buffer = new StringBuffer();
            BufferedReader reader = null;
            try {
                InputStream inputStream = con.getInputStream();
                reader = new BufferedReader(new InputStreamReader(inputStream));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                future.complete(new JsonObject(buffer.toString()));
            } catch (IOException e) {
                error("请求失败：%s", e.getMessage());
                future.fail("请求失败：" + buffer.toString());
            } finally {
                if (reader != null) {
                    reader.close();
                }
            }
        } catch (Exception e) {
            future.fail("请求失败：" + e.getMessage());
            error("请求失败：%s", e.getMessage());
        }
    }


    /**
     * 输出调试日志
     *
     * @param message
     * @param objects
     */
    void info(Object message, Object... objects) {
        LoggerFactory.getLogger(this.getClass()).info(message, objects);
    }

    /**
     * 输出错误日志
     *
     * @param message
     * @param objects
     */
    void error(Object message, Object... objects) {
        LoggerFactory.getLogger(this.getClass()).error(message, objects);
    }
}
