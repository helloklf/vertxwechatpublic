package fzs.wechat.shared.wechat.models;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;

/**
 * Created by Hello on 2017/4/7.
 */
public class WeChatApiInfo {
    public WeChatApiInfo(JsonObject jsonObject) {
        this.url = jsonObject.getString("url");
        this.event = jsonObject.getString("event", this.url);//如果不单独指定event，将直接使用url作为event
        this.desc = jsonObject.getString("desc", ""); //接口说明，没有实际用途
        this.method = (jsonObject.getString("method", "post").toLowerCase().equals("get")) ? HttpMethod.GET : HttpMethod.POST;
    }

    public String url;
    public String event;
    public String desc;
    public HttpMethod method;
}