package fzs.wechat.shared;

import fzs.wechat.shared.wechat.models.ServiceConfig;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

/**
 * 统一的事件、广播调用方式
 * Created by Hello on 2017/4/6.
 */
public class EventBusHelper {
    private EventBus eventBus;
    private String eventPrefix = null;

    /**
     * @param eventBus
     * @param eventPrefix 事件地址前缀，建议以公众号的域名开头，格式如 www.baidu.com
     */
    public EventBusHelper(EventBus eventBus, String eventPrefix) {
        this.eventBus = eventBus;
        this.eventPrefix = eventPrefix;
    }

    /**
     * 触发事件：点对点的事件，最多只有一个事件处理程序接收到此事件，且必须要做出回复，否则认为调用失败
     *
     * @param eventName      事件名称
     * @param jsonObject     数据
     * @param requestHandler 回调
     */
    public void triggerEvent(String eventName, JsonObject jsonObject, Handler<AsyncResult<Message<JsonObject>>> requestHandler) {
        if (jsonObject == null)
            jsonObject = new JsonObject();

        eventBus.send(formatEventAddress(eventName), jsonObject, requestHandler);
    }

    /**
     * 触发事件：点对点的事件，最多只有一个事件处理程序接收到此事件，且必须要做出回复，否则认为调用失败
     *
     * @param eventName       事件名称
     * @param jsonObject      数据
     * @param deliveryOptions 传递设置
     * @param requestHandler  回调
     */
    public void triggerEvent(String eventName, JsonObject jsonObject, DeliveryOptions deliveryOptions, Handler<AsyncResult<Message<JsonObject>>> requestHandler) {
        if (jsonObject == null)
            jsonObject = new JsonObject();

        eventBus.send(formatEventAddress(eventName), jsonObject, deliveryOptions, requestHandler);
    }

    /**
     * 发布事件：广播形式的事件，抛出事件和数据内容而不关心响应情况
     *
     * @param eventName  事件名称
     * @param jsonObject 数据
     */
    public void pulishEvent(String eventName, JsonObject jsonObject) {
        if (jsonObject == null)
            jsonObject = new JsonObject();

        eventBus.publish(formatEventAddress(eventName), jsonObject);
    }


    /**
     * 格式化事件地址
     *
     * @param eventName
     * @return
     */
    private String formatEventAddress(String eventName) {
        return EventBusHelper.formatEventAddress(this.eventPrefix,eventName);
    }

    /**
     * 格式化事件地址
     *
     * @param serviceConfig
     * @param eventName
     * @return
     */
    public static String formatEventAddress(ServiceConfig serviceConfig, String eventName) {
        String eventPrefix = getEventPrefix(serviceConfig);
        return formatEventAddress(eventPrefix, eventName);
    }

    /**
     * 格式化事件地址
     *
     * @param eventPrefix
     * @param eventName
     * @return
     */
    public static String formatEventAddress(String eventPrefix, String eventName) {
        if(eventName==null)
            return "unknown_address";

        if (eventPrefix == null || eventPrefix.length() == 0)
            return (eventName.startsWith(".") ? eventName.substring(1) : eventName).toLowerCase();
        return String.format("%s->%s", eventPrefix, eventName.startsWith(".") ? eventName.substring(1) : eventName).toLowerCase();
    }

    /**
     * 获取事件地址前缀
     *
     * @param serviceConfig
     * @return
     */
    public static String getEventPrefix(ServiceConfig serviceConfig) {
        return serviceConfig.getMicroServiceConfig().host;
    }
}
