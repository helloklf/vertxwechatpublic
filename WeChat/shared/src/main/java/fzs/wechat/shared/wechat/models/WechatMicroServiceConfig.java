package fzs.wechat.shared.wechat.models;

import io.vertx.core.json.JsonObject;

/**
 * Created by Hello on 2017/4/7.
 */
public class WechatMicroServiceConfig {
    public String appid;
    public String appSecret;
    public String token;
    public String host;

    public WechatMicroServiceConfig(JsonObject jsonObject) {
        appid = jsonObject.getString("appid");
        appSecret = jsonObject.getString("appSecret");
        token = jsonObject.getString("token");
        host = jsonObject.getString("host", "").replace("http://", "").replace("https://", "").replace("/", "");
    }
}
