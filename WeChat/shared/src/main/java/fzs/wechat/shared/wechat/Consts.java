package fzs.wechat.shared.wechat;

/**
 * Created by Hello on 2017/4/5.
 */
public class Consts {
    //微服务集群已添加
    public static final String MICRO_SERVICE_CLUSTER_ADDED = "fzs.wechat.cluster.added";

    //微服务集群已移除
    public static final String MICRO_SERVICE_CLUSTER_LEFT = "fzs.wechat.cluster.left";

    //初始化配置事件地址
    public static final String INIT_EVENT_ADDRESS = "fzs.wechat.config";

    //移除配置事件地址
    public static final String UNINIT_EVENT_ADDRESS = "fzs.wechat.unconfig";

    //微信验证介入事件地址
    public static final String VALIDATE_EVENT_ADDRESS = "fzs.wechat.validate";

    //获取jsticket事件
    public static final String MICRO_SERVICE_GETJSTICKET_EVENT = "getjsticket";
}
