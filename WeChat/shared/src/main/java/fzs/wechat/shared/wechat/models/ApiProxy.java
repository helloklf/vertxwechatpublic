package fzs.wechat.shared.wechat.models;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;

/**
 * Created by Hello on 2017/4/7.
 */
public class ApiProxy {
    public ApiProxy(JsonObject jsonObject) {
        JsonArray array = jsonObject.getJsonArray("ignore", new JsonArray());

        this.ignore = new ArrayList<>();
        array.forEach(item -> {
            if (item != null && !item.toString().trim().equals(""))
                this.ignore.add(item.toString());
        });
        this.fileField = jsonObject.getString("files_field", "files");
    }

    //忽略参数列表
    public ArrayList<String> ignore;
    //文件上传所用字段
    public String fileField;
}