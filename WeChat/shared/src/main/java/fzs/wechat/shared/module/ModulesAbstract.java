package fzs.wechat.shared.module;

import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

import java.util.Map;

/**
 * 功能模块结构：定义一组点对点的通讯的事件响应
 * 功能模块所包含的功能实现应是 "eventName":function(...params){  }的方式
 * 并实现getRegisterInfos方法来实现返回单前模块所有事件和方法调用对应关系
 * Created by Hello on 2017/3/31.
 */
public interface ModulesAbstract {
    Map<String, Handler<Message<JsonObject>>> getEvents();
}
