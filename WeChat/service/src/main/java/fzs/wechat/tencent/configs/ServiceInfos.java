package fzs.wechat.tencent.configs;

import fzs.wechat.shared.wechat.models.ServiceConfig;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 * Created by Hello on 2017/4/12.
 */
public class ServiceInfos {
    public static Map<String,ServiceConfig> map = new HashMap<>();

    public void getServiceConfigs(Handler<ServiceConfig[]> handler){
        ServiceConfig[] arrayList = new ServiceConfig[map.size()];
        handler.handle(map.values().toArray(arrayList));
    }

    public void getServiceConfigInfo(String host, Handler<ServiceConfig> handler){
        //TODO：实际上配置信息应该从数据库或其它位置读取，而非在代码中写
        if(map.containsKey(host))
            handler.handle(map.get(host));
        else
            handler.handle(null);
    }

    public void getServiceHosts(Handler<String[]> handler){
        handler.handle((String[]) (map.keySet().toArray()));
    }
}
