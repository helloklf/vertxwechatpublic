package fzs.wechat.tencent;

import fzs.wechat.shared.EventBusHelper;
import fzs.wechat.shared.module.ModulesManager;
import fzs.wechat.shared.wechat.Consts;
import fzs.wechat.shared.wechat.models.ServiceConfig;
import fzs.wechat.tencent.configs.ServiceInfos;
import fzs.wechat.tencent.modules.MicroServiceManageModule;
import io.vertx.core.*;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.core.spi.cluster.NodeListener;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by WEI on 2017/3/8.
 */
public class MainVerticle extends AbstractVerticle {
    @Override
    public void start(Future<Void> startFuture) throws Exception {
        //Logger.getLogger("com.hazelcast").setLevel(Level.WARNING);

        if(!vertx.isClustered()){
            JsonObject jsonObject = config();
            ServiceInfos.map = new HashMap<>();
            for(Map.Entry<String,Object> item:jsonObject) {
                ServiceInfos.map.put(item.getKey(), new ServiceConfig((JsonObject) (item.getValue())));
            }
            deployAsCluster(startFuture);
        }
        else {
        }
    }

    private void deployAsCluster(Future<Void> startFuture){
        ClusterManager mgr = new HazelcastClusterManager();
        VertxOptions options = new VertxOptions().setClusterManager(mgr);

        Vertx.clusteredVertx(options, res -> {
            if (res.succeeded()) {
                Vertx vertx = res.result();

                final EventBusHelper eventBusHelper = new EventBusHelper(vertx.eventBus(), "");
                mgr.nodeListener(new NodeListener() {
                    @Override
                    public void nodeAdded(String nodeID) {
                        eventBusHelper.pulishEvent(Consts.MICRO_SERVICE_CLUSTER_ADDED, new JsonObject().put("cluster", nodeID));
                    }

                    @Override
                    public void nodeLeft(String nodeID) {
                        eventBusHelper.pulishEvent(Consts.MICRO_SERVICE_CLUSTER_LEFT, new JsonObject().put("cluster", nodeID));
                    }
                });

                initConfigs(startFuture,vertx);
            } else {
                LoggerFactory.getLogger(MainVerticle.class).error("部署集群遇到问题，所以程序未能成功启动");
                startFuture.fail(res.cause());
            }
        });
    }

    private void initConfigs(Future<Void> startFuture, Vertx vertx){
        try {
            startFuture.complete();
            new ModulesManager("", vertx.eventBus()).registerModule(new MicroServiceManageModule(vertx));
        } catch (Exception ex) {
            startFuture.fail(ex.getCause());
        }

        new TencentRouterConfigurator(vertx).config(config().getInteger("http.port", 80));
    }
}
