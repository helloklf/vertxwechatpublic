package fzs.wechat.tencent;

import fzs.wechat.shared.tools.HttpUploadFile;
import fzs.wechat.shared.tools.Tools;
import fzs.wechat.shared.wechat.Consts;
import fzs.wechat.shared.wechat.models.ServiceConfig;
import fzs.wechat.shared.wechat.models.WeChatApiInfo;
import fzs.wechat.shared.wechat.models.WechatMicroServiceConfig;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;

import java.util.ArrayList;

/**
 * Created by Hello on 2017/3/17.
 */
public class WechatApiProxyVerticle extends AbstractVerticle {
    private static final HttpClientOptions HTTP_CLIENT_OPTIONS = new HttpClientOptions()
        .setDefaultHost("api.weixin.qq.com")
        .setSsl(true)
        .setDefaultPort(443);
    ServiceConfig serviceConfig;
    HttpClient httpClient;
    String accessToken;
    EventBus eventBus;
    String jsTicket;
    ArrayList<MessageConsumer> consumerList = new ArrayList<>();

    public WechatApiProxyVerticle(ServiceConfig serviceConfig) {
        this.serviceConfig = serviceConfig;
    }

    @Override
    public void start(Future<Void> startFuture) {
        this.eventBus = vertx.eventBus();
        Future<Void> future = Future.<Void>future();
        this.httpClient = vertx.createHttpClient(HTTP_CLIENT_OPTIONS);

        future.setHandler(h -> {
            if (h.succeeded()) {
                getJsTicket();
                subscribeEvents(eventBus, serviceConfig);
                startFuture.complete();
            } else
                startFuture.fail("部署api请求代理失败，无法获取accesstoken，请检查配置是否有误");
        });

        getAccessToken(serviceConfig.getMicroServiceConfig(), future);
    }

    @Override
    public void stop(Future<Void> stopFuture) throws Exception {
        stop();
        unsubscribeEvents();
        stopFuture.complete();
    }


    /**
     * 订阅接口请求事件
     *
     * @param eventBus
     * @param serviceConfig
     */
    void subscribeEvents(EventBus eventBus, ServiceConfig serviceConfig) {
        String eventPrefix = serviceConfig.getMicroServiceConfig().host;
        ArrayList<WeChatApiInfo> apiInfos = serviceConfig.getWechatApi();
        apiInfos.forEach(
            apiInfo -> {
                MessageConsumer consumer = eventBus.consumer(eventPrefix + "->" + apiInfo.event, requestMessage -> {
                    Future<JsonObject> responseFuture = Future.future();
                    String url = apiInfo.url;
                    responseFuture.setHandler(h -> {
                        if (h.succeeded())
                            requestMessage.reply(h.result());
                        else
                            requestMessage.fail(500, "请求发送失败");
                    });
                    httpRequest(url, apiInfo.method, (JsonObject) requestMessage.body(), responseFuture);
                });
                consumerList.add(consumer);
            }
        );

        /**
         * 授权信息传送
         */
        MessageConsumer consumer = eventBus.consumer(eventPrefix + "->" + Consts.MICRO_SERVICE_GETJSTICKET_EVENT, requestMessage -> {
            requestMessage.reply(new JsonObject().put("jsticket", jsTicket));
        });
        consumerList.add(consumer);
    }

    /**
     * 解除接口订阅事件
     */
    void unsubscribeEvents() {
        for (int i = 0; i < consumerList.size(); i++) {
            consumerList.get(i).unregister();
        }
    }

    /**
     * 发起http请求
     *
     * @param api           请求地址
     * @param method        请求方法
     * @param requestParams 请求参数
     */
    void httpRequest(String api, HttpMethod method, JsonObject requestParams, Future<JsonObject> responseFuture) {
        String url = Tools.urlFormate(api, new JsonObject().put("access_token", this.accessToken));

        //如果是get请求，则将其它请求参数组合到url中
        if (method == HttpMethod.GET && requestParams != null)
            url = Tools.urlFormate(url, requestParams);

        //文件上传特殊处理
        if (requestParams != null && requestParams.containsKey("files")) {
            byte[] bytes = Tools.base64Decode(requestParams.getJsonArray("files").getString(0));
            JsonObject params = requestParams.copy();
            params.remove("files");
            String filename = params.getString("filename", "");
            params.remove("filename");
            new HttpUploadFile().uploadFile("https://api.weixin.qq.com" + url, filename, bytes, params, responseFuture);
        } else {
            HttpClientRequest httpClientRequest = this.httpClient.request(method, url, response -> {
                if (response.statusCode() == 200) {
                    response.bodyHandler(buffer -> {
                        String str = buffer.getString(0, buffer.length());
                        JsonObject jsonObject = new JsonObject(str);
                        info("请求 {0} with {1} 结果: {2}", api, requestParams, jsonObject);
                        responseFuture.complete(jsonObject);
                    });
                } else {
                    responseFuture.fail("request failed");
                }
            });
            httpClientRequest.exceptionHandler(e -> {
                responseFuture.fail("exception: " + e.getMessage());
            });

            if (requestParams != null)
                httpClientRequest.end(requestParams.encode());
            else
                httpClientRequest.end();
        }
    }

    /**
     * 获取AccessToken 并创建定时任务，定期刷新（AccessToken有效期为7200秒）
     *
     * @param wechatMicroServiceConfig
     * @param future
     */
    private void getAccessToken(WechatMicroServiceConfig wechatMicroServiceConfig, Future future) {
        String url = Tools.urlFormate("/cgi-bin/token?grant_type=client_credential",
            new JsonObject().put("appid", wechatMicroServiceConfig.appid).put("secret", wechatMicroServiceConfig.appSecret));

        HttpClientRequest httpClientRequest = this.httpClient.get(url, response -> {
            if (response.statusCode() == 200) {
                response.bodyHandler(buffer -> {
                    JsonObject jsonObject = new JsonObject(buffer.getString(0, buffer.length()));
                    int errcode = jsonObject.getInteger("errcode", 0);
                    if (errcode == 0) {
                        this.accessToken = jsonObject.getString("access_token");
                        Integer expiresIn = jsonObject.getInteger("expires_in");
                        info("accessToken refreshed: {0}", accessToken);
                        vertx.setTimer((expiresIn - 10) * 1000, id -> getAccessToken(wechatMicroServiceConfig, null));
                        future.complete();
                    } else {
                        String errmsg = jsonObject.getString("errmsg");
                        error("access token返回错误, errcode: {0}, errmsg: {1}", errcode, errmsg);
                        future.fail(String.format("access token返回错误, errcode: {0}, errmsg: {1}", errcode, errmsg));
                    }
                });
            } else {
                error("access token请求失败, 错误状态码: {0}", response.statusCode());
                future.fail(String.format("access token请求失败, 错误状态码: {0}", response.statusCode()));
            }
        });
        httpClientRequest.exceptionHandler(e -> {
            error("获取access token错误: {0}", e.getMessage());
            future.fail(String.format("获取access token错误: {0}", e.getMessage()));
        });
        httpClientRequest.end();
    }

    /**
     * 获取jssdk ticket 并创建定时任务，定期刷新（ticket有效期为7200秒）
     */
    private void getJsTicket() {
        JsonObject params = new JsonObject().put("access_token", accessToken).put("type", "jsapi");

        String url = Tools.urlFormate("/cgi-bin/ticket/getticket", params);

        HttpClientRequest httpClientRequest = this.httpClient.get(url, response -> {
            if (response.statusCode() == 200) {
                response.bodyHandler(buffer -> {
                    JsonObject jsonObject = new JsonObject(buffer.getString(0, buffer.length()));
                    int errcode = jsonObject.getInteger("errcode", 0);
                    if (errcode == 0) {
                        this.jsTicket = jsonObject.getString("ticket");
                        Integer expiresIn = jsonObject.getInteger("expires_in");
                        info("jssdk ticket refreshed: {0}", jsTicket);
                        vertx.setTimer((expiresIn - 10) * 1000, id -> getJsTicket());
                    } else {
                        String errmsg = jsonObject.getString("errmsg");
                        error("jssdk ticket返回错误, errcode: {0}, errmsg: {1}", errcode, errmsg);
                    }
                });
            } else {
                error("jssdk ticket请求失败, 错误状态码: {0}", response.statusCode());
            }
        });
        httpClientRequest.exceptionHandler(e -> error("获取jssdk ticket错误: {0}", e.getMessage()));
        httpClientRequest.end();
    }

    /**
     * 输出调试日志
     *
     * @param message
     * @param objects
     */
    void info(Object message, Object... objects) {
        LoggerFactory.getLogger(this.getClass()).info(message, objects);
    }

    /**
     * 输出错误日志
     *
     * @param message
     * @param objects
     */
    void error(Object message, Object... objects) {
        LoggerFactory.getLogger(this.getClass()).error(message, objects);
    }
}