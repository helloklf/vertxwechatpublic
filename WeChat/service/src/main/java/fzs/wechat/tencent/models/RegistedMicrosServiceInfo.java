package fzs.wechat.tencent.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hello on 2017/4/7.
 */
public class RegistedMicrosServiceInfo {
    public RegistedMicrosServiceInfo(String host,String token,String clusterID,String apiProxyVerticleID) {
        this.host = host;
        this.token = token;
        this.clusterIDs = new ArrayList<String>(){{
            if(clusterID!=null)
                add(clusterID);
        }};
        this.apiProxyVerticleID = apiProxyVerticleID;
    }

    //公众号绑定域名
    private String host;
    //公众号token
    private String token;
    //部署的集群
    private List<String> clusterIDs;
    //部署的代理
    private String apiProxyVerticleID;

    public String getHost(){
        return this.host;
    }

    public String getToken(){
        return this.token;
    }

    public List<String> getClusterIDs(){
        return this.clusterIDs;
    }

    public void putClusterID(String clusterID){
        this.clusterIDs.add(clusterID);
    }

    public void removeClusterID(String clusterID){
        if(this.clusterIDs.contains(clusterID))
            this.clusterIDs.remove(clusterID);
    }

    public String getApiProxyVerticleID(){
        return this.apiProxyVerticleID;
    }
}
