package fzs.wechat.tencent;

import com.googlecode.concurrentlinkedhashmap.ConcurrentLinkedHashMap;
import fzs.wechat.shared.EventBusHelper;
import fzs.wechat.shared.module.ModulesManager;
import fzs.wechat.shared.tools.Tools;
import fzs.wechat.shared.wechat.Consts;
import fzs.wechat.shared.wechat.MessageType;
import fzs.wechat.tencent.modules.MicroServiceManageModule;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.core.spi.cluster.NodeListener;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import java.util.List;
import java.util.Map;

/**
 * Created by WEI on 2017/3/4.
 */
public class TencentRouterConfigurator {
    private Vertx vertx;
    private EventBus eventBus;
    //lru的特性是不必要的，可以替换成更加合适的"有容量上限的数据结构"。
    ConcurrentLinkedHashMap<String, JsonObject> messageLru = new ConcurrentLinkedHashMap.Builder<String, JsonObject>()
        .maximumWeightedCapacity(1024)
        .build();

    public TencentRouterConfigurator(Vertx vertx) {
        this.eventBus = vertx.eventBus();
        this.vertx = vertx;
    }

    /**
     * 配置路由
     * 主服务将占用80端口，因为微信公众号要求必须使用此端口，但你也可以将此端口用于其它用途
     * @param port
     */
    public void config(int port) {
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.route("/*").handler(routingContext -> {
            HttpMethod method = routingContext.request().method();
            //接入验证
            if (method == HttpMethod.GET)
                validate(routingContext);
            else if (method == HttpMethod.POST)
                forwardedMessage(routingContext);
            else
                routingContext.fail(401);
        });

        vertx.createHttpServer().requestHandler(router::accept).listen(port);
    }

    /**
     * 微信认证
     *
     * @param routingContext
     */
    private void validate(RoutingContext routingContext) {
        DeliveryOptions deliveryOptions = new DeliveryOptions();
        deliveryOptions.setHeaders(routingContext.request().headers());

        List<Map.Entry<String, String>> params = routingContext.request().params().entries();
        JsonObject jsonObject = new JsonObject();
        for (int i = 0; i < params.size(); i++) {
            jsonObject.put(params.get(i).getKey(), params.get(i).getValue());
        }

        new EventBusHelper(eventBus, "").triggerEvent(
            Consts.VALIDATE_EVENT_ADDRESS,
            jsonObject,
            deliveryOptions,
            (AsyncResult<Message<JsonObject>> r) -> {
                if (r.succeeded()) {
                    routingContext.response().end(r.result().body().getString("result"));
                } else {
                    routingContext.fail(r.cause());
                }
            }
        );
    }

    /**
     * 消息转发
     *
     * @param routingContext
     */
    public void forwardedMessage(RoutingContext routingContext) {
        String body = routingContext.getBodyAsString();
        JsonObject requestInfo = null;
        try {
            if (body.startsWith("<xml"))
                requestInfo = Tools.parseXml(body);
            else
                requestInfo = new JsonObject(body);
        } catch (Exception ex) {
        } finally {
            if (requestInfo == null) {
                routingContext.response().end("无法解析为xml或json对象，请检查数据格式，并将参数以字符串形式在请求body部分传递！");
                return;
            }
        }
        final JsonObject request = requestInfo;

        String host = routingContext.request().getHeader("host");
        String eventName = getEventName(request);
        long msgId = request.getLong("msgId", (long) -1);
        String key = String.format("%s:%d", host, msgId);
        if (msgId > -1) {
            //不使用LRU的特性，所以这里不使用get来表示“命中缓存”
            JsonObject cachedResponse = this.messageLru.getQuietly(key);
            if (cachedResponse != null) {
                routingContext.response().end(Tools.encodeXml(cachedResponse));
                return;
            }
        }
        if (eventName == null) {
            routingContext.fail(500);
            return;
        }

        //消息传递设置：传递超时时间为4秒
        //* 微信服务器在将用户的消息发给公众号的开发者服务器地址（开发者中心处配置）后，微信服务器在五秒内收不到响应会断掉连接，并且重新发起请求，总共重试三次，
        //此处设置超时时间为15秒，避免在传递过程中由于不回复或者其它原因导致的无限等待
        DeliveryOptions deliveryOptions = new DeliveryOptions() {{
            setSendTimeout(15000);
        }};
        //设置消息标头
        deliveryOptions.setHeaders(routingContext.request().headers());

        new EventBusHelper(eventBus, host).triggerEvent(
            eventName,
            request,
            deliveryOptions,
            ar -> {
                HttpServerResponse httpServerResponse = routingContext.response().putHeader("Access-Control-Allow-Origin", "*");
                if (ar.succeeded()) {
                    Object result = ar.result().body();
                    if (result == null || result.toString().trim().length() == 0) {
                        httpServerResponse.end("");
                        info("response empty");
                    } else {
                        JsonObject response = fillResponse(request, (JsonObject) result);
                        if (response == null) {
                            httpServerResponse.end("");
                            error("s%：response msgtype unknown", eventName);
                            return;
                        } else {
                            String xml = Tools.encodeXml(response);
                            httpServerResponse.end(xml);
                            info("response xml: {0}", xml);

                            if (msgId > -1)
                                this.messageLru.put(key, response);
                        }
                    }
                } else {
                    info("response fail: {0}", ar.cause().getMessage());
                    httpServerResponse.end("success");
                }
            }
        );
    }

    private String getEventName(JsonObject request) {
        String msgType = request.getString("msgType", "unknown").toLowerCase();
        String event = request.getString("event", null);
        return msgType + (event != null ? "." + event.toLowerCase() : "");
    }

    private JsonObject fillResponse(JsonObject request, JsonObject result) {
        JsonObject response = new JsonObject();
        String toUserName = request.getString("toUserName");
        String fromUserName = request.getString("fromUserName");
        response.put("toUserName", wrapCdata(fromUserName));
        response.put("fromUserName", wrapCdata(toUserName));
        response.put("createTime", System.currentTimeMillis() / 1000);
        MessageType msgType = MessageType.text;
        try {
            msgType = MessageType.valueOf(result.getString("msgType").toLowerCase());
        } catch (Exception ex) {
            return null;
        }

        response.put("msgType", wrapCdata(result.getString("msgType")));
        switch (msgType) {
            case text: {
                response.put("content", wrapCdata(result.getString("content")));
                break;
            }
            case image: {
                response.put("image", "<MediaId>" + wrapCdata(result.getString("mediaId")) + "</MediaId>");
                break;
            }
            case news: {
                JsonArray articles = result.getJsonArray("articles");
                response.put("articleCount", wrapCdata("" + articles.size()));
                StringBuilder stringBuilder = new StringBuilder();
                articles.forEach(item -> {
                    JsonObject jsonObject = (JsonObject) item;
                    stringBuilder.append(String.format("<item>\n" +
                            "<Title><![CDATA[%s]]></Title> \n" +
                            "<Description><![CDATA[%s]]></Description>\n" +
                            "<PicUrl><![CDATA[%s]]></PicUrl>\n" +
                            "<Url><![CDATA[%s]]></Url>\n" +
                            "</item>",
                        jsonObject.getString("title"),
                        jsonObject.getString("desc"),
                        jsonObject.getString("picUrl"),
                        jsonObject.getString("url")));
                });
                response.put("Articles", stringBuilder.toString());
                break;
            }
            case voice: {
                response.put("voice", "<MediaId>" + wrapCdata(result.getString("mediaId")) + "</MediaId>");
                break;
            }
            case video: {
                response.put("video",
                    "<MediaId>" + wrapCdata(result.getString("mediaId")) + "</MediaId>" +
                        "<Title>" + wrapCdata(result.getString("title")) + "</Title>" +
                        "<Description>" + wrapCdata(result.getString("description")) + "</Description>"
                );
                break;
            }
            case music: {
                response.put("music",
                    "<Title>" + wrapCdata(result.getString("title")) + "</Title>" +
                        "<ThumbMediaId>" + wrapCdata(result.getString("thumbMediaId")) + "</ThumbMediaId>" +
                        "<MusicUrl>" + wrapCdata(result.getString("musicUrl")) + "</MusicUrl>" +
                        "<Description>" + wrapCdata(result.getString("description")) + "</Description>" +
                        "<HQMusicUrl>" + wrapCdata(result.getString("hQMusicUrl")) + "</HQMusicUrl>"
                );
                break;
            }
        }
        return response;
    }

    private String wrapCdata(String content) {
        //微信的回复不需要XML.escape
        return "<![CDATA[" + content + "]]>";
    }

    /**
     * 输出调试日志
     *
     * @param message
     * @param objects
     */
    private void info(Object message, Object... objects) {
        LoggerFactory.getLogger(this.getClass()).info(message, objects);
    }

    /**
     * 输出错误日志
     *
     * @param message
     * @param objects
     */
    private void error(Object message, Object... objects) {
        LoggerFactory.getLogger(this.getClass()).error(message, objects);
    }
}
