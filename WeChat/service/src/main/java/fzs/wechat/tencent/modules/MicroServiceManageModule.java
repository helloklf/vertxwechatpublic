package fzs.wechat.tencent.modules;

import fzs.wechat.shared.EventBusHelper;
import fzs.wechat.shared.module.ModulesAbstract;
import fzs.wechat.shared.wechat.Consts;
import fzs.wechat.shared.wechat.models.ServiceConfig;
import fzs.wechat.tencent.WechatApiProxyVerticle;
import fzs.wechat.tencent.configs.ServiceInfos;
import fzs.wechat.tencent.models.RegistedMicrosServiceInfo;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Hello on 2017/4/6.
 */
public class MicroServiceManageModule implements ModulesAbstract {
    private static Vertx vertx;
    private static Map<String, RegistedMicrosServiceInfo> registedMicrosServiceInfoMap = new HashMap<>();

    public MicroServiceManageModule(Vertx vertx) {
        this.vertx = vertx;
    }

    @Override
    public Map<String, Handler<Message<JsonObject>>> getEvents() {
        HashMap<String, Handler<Message<JsonObject>>> config = new HashMap<>();

        config.put(Consts.MICRO_SERVICE_CLUSTER_ADDED, this::addMicroServiceClusterNode);
        config.put(Consts.MICRO_SERVICE_CLUSTER_LEFT, this::removeMicroServiceClusterNode);

        new ServiceInfos().getServiceConfigs(r -> {
            for (int i = 0; i < r.length; i++) {
                config.put(EventBusHelper.formatEventAddress(r[i], Consts.INIT_EVENT_ADDRESS), this::registerMicroService);
                config.put(EventBusHelper.formatEventAddress(r[i], Consts.UNINIT_EVENT_ADDRESS), this::unregisterMicroService);
            }
        });

        config.put(Consts.VALIDATE_EVENT_ADDRESS, this::validate);

        return config;
    }

    /**
     * 获取微信公众号Token
     *
     * @param host
     * @return
     */
    public String getMicroServiceTokenByHost(String host) {
        return registedMicrosServiceInfoMap.get(host).getToken();
    }

    public void addMicroServiceClusterNode(Message<JsonObject> message) {
        String clusterNodeID = message.body().getString("cluster");
        info("集群节点已加入连接：" + clusterNodeID);
    }

    /**
     * 移除微服务集群节点：在集群节点被动断开连接时触发
     *
     * @param message
     */
    public void removeMicroServiceClusterNode(Message<JsonObject> message) {
        String clusterNodeID = message.body().getString("cluster");
        removeMicroServiceClusterNode(clusterNodeID);

        message.reply("ok");
    }

    private void removeMicroServiceClusterNode(String clusterNodeID){Iterator iterator = registedMicrosServiceInfoMap.entrySet().iterator();
        while (iterator.hasNext()) {
            RegistedMicrosServiceInfo item = ((Map.Entry<String, RegistedMicrosServiceInfo>) iterator.next()).getValue();
            if (item.getClusterIDs().contains(clusterNodeID)) {
                String host = item.getHost();
                RegistedMicrosServiceInfo registedMicrosServiceInfo = registedMicrosServiceInfoMap.get(host);
                registedMicrosServiceInfo.removeClusterID(clusterNodeID);
                if (registedMicrosServiceInfo.getClusterIDs().size() == 0) {
                    vertx.undeploy(registedMicrosServiceInfo.getApiProxyVerticleID());
                    registedMicrosServiceInfoMap.remove(host);
                }
                break;
            }
        }
        info("集群节点已断开连接：" + clusterNodeID);
    }

    /**
     * 注册微服务
     *
     * @param message
     */
    public void registerMicroService(Message<JsonObject> message) {
        String host = message.body().getString("host");

        new ServiceInfos().getServiceConfigInfo(
            host,
            serviceConfig -> {
                String cluster = message.body().getString("cluster", null);
                if (registedMicrosServiceInfoMap.containsKey(host)) {
                    if (cluster != null)
                        registedMicrosServiceInfoMap.get(host).putClusterID(cluster);

                    message.reply("ok");
                    return;
                }

                vertx.deployVerticle(
                    new WechatApiProxyVerticle(serviceConfig),
                    deployResult -> {
                        if (deployResult.succeeded()) {
                            registedMicrosServiceInfoMap.put(
                                host,
                                new RegistedMicrosServiceInfo(
                                    host,
                                    serviceConfig.getMicroServiceConfig().token,
                                    cluster,
                                    deployResult.result()
                                )
                            );
                            message.reply("ok");
                        } else {
                            message.fail(500, "部署代理失败！");
                        }
                    });
            });
    }

    /**
     * 反注册微服务
     *
     * @param message
     */
    public void unregisterMicroService(Message<JsonObject> message) {
        String cluster = message.body().getString("cluster", null);
        if(cluster!=null){
            removeMicroServiceClusterNode(cluster);
            return;
        }

        String host = message.body().getString("host");
        new ServiceInfos().getServiceConfigInfo(
            host, serviceConfig -> {
                if (registedMicrosServiceInfoMap.containsKey(host)) {
                    String apiProxyVerticleID = registedMicrosServiceInfoMap.get(host).getApiProxyVerticleID();
                    registedMicrosServiceInfoMap.remove(host);

                    vertx.undeploy(apiProxyVerticleID, r -> {
                        if (r.succeeded()) {
                            message.reply("成功！");
                        } else {
                            message.fail(500, "注销服务失败！");
                        }
                    });
                } else {
                    message.reply("成功！");
                }
            }
        );
    }

    /**
     * 处理微信验证接入
     *
     * @param message
     */
    public void validate(Message<JsonObject> message) {
        String host = message.headers().get("host");
        String token = getMicroServiceTokenByHost(host);
        JsonObject request = message.body();
        if (token == null) {
            //没有对应该host的token
            error("未设置 {0} 对应的token", host);
            message.fail(401, String.format("未设置 {0} 对应的token", host));
            return;
        }

        String signature = request.getString("signature");
        String timestamp = request.getString("timestamp");
        String nonce = request.getString("nonce");
        String echostr = request.getString("echostr");

        if (StringUtils.isNotEmpty(signature) && StringUtils.isNotEmpty(timestamp) && StringUtils.isNotEmpty(nonce) && StringUtils.isNotEmpty(echostr)) {
            String[] array = new String[]{token, timestamp, nonce};
            Arrays.sort(array);
            String temp = StringUtils.join(Arrays.asList(array), "");
            temp = DigestUtils.sha1Hex(temp);
            boolean isValid = temp.equals(signature);
            if (isValid) {
                message.reply(new JsonObject().put("result", echostr));
                return;
            }
        }
        message.fail(401, "此请求不是来自微信！");
    }

    /**
     * 输出调试日志
     *
     * @param message
     * @param objects
     */
    void info(Object message, Object... objects) {
        LoggerFactory.getLogger(this.getClass()).info(message, objects);
    }

    /**
     * 输出错误日志
     *
     * @param message
     * @param objects
     */
    void error(Object message, Object... objects) {
        LoggerFactory.getLogger(this.getClass()).error(message, objects);
    }
}
