package fzs.wechat.binder.models.response;

/**
 * Created by WEI on 2017/3/9.
 */
public class TextResponse extends WechatResponse {
    public TextResponse() {
        super(MessageType.text);
    }

    String content;

    public String getContent() {
        return content;
    }

    public TextResponse setContent(String content) {
        this.content = content;
        return this;
    }
}
