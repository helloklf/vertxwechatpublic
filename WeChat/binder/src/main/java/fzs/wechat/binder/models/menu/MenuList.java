package fzs.wechat.binder.models.menu;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Created by Hello on 2017/4/6.
 */
public abstract class MenuList {
    JsonArray menus;

    public MenuList() {
        menus = new JsonArray();
    }

    /**
     * 添加时间菜单按钮
     *
     * @param text     显示的文字-最多7字-多余的将使用省略号形式显示
     * @param eventKey 唯一key
     */
    public MenuList addEventMenu(String text, String eventKey) {
        menus.add(new JsonObject().put("type", "click").put("name", text).put("key", eventKey));
        return this;
    }

    /**
     * 添加页面链接按钮
     *
     * @param text
     * @param url
     */
    public MenuList addViewMenu(String text, String url) {
        menus.add(new JsonObject().put("type", "view").put("name", text).put("url", url));
        return this;
    }

    /**
     * 扫码推事件
     * 用户点击按钮后，微信客户端将调起扫一扫工具，完成扫码操作后显示扫描结果（如果是URL，将进入URL），且会将扫码的结果传给开发者，开发者可以下发消息。
     *
     * @param text
     * @param eventKey
     */
    public MenuList addCcancodePushMenu(String text, String eventKey) {
        menus.add(new JsonObject().put("type", "scancode_push").put("name", text).put("key", eventKey));
        return this;
    }

    /**
     * 扫码推事件且弹出“消息接收中”提示框
     * 用户点击按钮后，微信客户端将调起扫一扫工具，完成扫码操作后，将扫码的结果传给开发者，同时收起扫一扫工具，然后弹出“消息接收中”提示框，随后可能会收到开发者下发的消息。
     *
     * @param text
     * @param eventKey
     */
    public MenuList addScancodeWaitmsgMenu(String text, String eventKey) {
        menus.add(new JsonObject().put("type", "scancode_waitmsg").put("name", text).put("key", eventKey));
        return this;
    }

    /**
     * 弹出系统拍照发图
     * 用户点击按钮后，微信客户端将调起系统相机，完成拍照操作后，会将拍摄的相片发送给开发者，并推送事件给开发者，同时收起系统相机，随后可能会收到开发者下发的消息。
     *
     * @param text
     * @param eventKey
     */
    public MenuList addPicSysphotoMenu(String text, String eventKey) {
        menus.add(new JsonObject().put("type", "pic_sysphoto").put("name", text).put("key", eventKey));
        return this;
    }

    /**
     * 弹出拍照或者相册发图
     * 用户点击按钮后，微信客户端将弹出选择器供用户选择“拍照”或者“从手机相册选择”。用户选择后即走其他两种流程。
     *
     * @param text
     * @param eventKey
     */
    public MenuList addPicPhoto_or_albumMenu(String text, String eventKey) {
        menus.add(new JsonObject().put("type", "pic_photo_or_album").put("name", text).put("key", eventKey));
        return this;
    }

    /**
     * 弹出微信相册发图器
     * 用户点击按钮后，微信客户端将调起微信相册，完成选择操作后，将选择的相片发送给开发者的服务器，并推送事件给开发者，同时收起相册，随后可能会收到开发者下发的消息。
     *
     * @param text
     * @param eventKey
     */
    public MenuList addPicWeixinMenu(String text, String eventKey) {
        menus.add(new JsonObject().put("type", "pic_weixin").put("name", text).put("key", eventKey));
        return this;
    }

    /**
     * 弹出地理位置选择器
     * 用户点击按钮后，微信客户端将调起地理位置选择工具，完成选择操作后，将选择的地理位置发送给开发者的服务器，同时收起位置选择工具，随后可能会收到开发者下发的消息。
     *
     * @param text
     * @param eventKey
     */
    public MenuList addLocationSelectMenu(String text, String eventKey) {
        menus.add(new JsonObject().put("type", "location_select").put("name", text).put("key", eventKey));
        return this;
    }

    /**
     * 下发消息（除文本消息）
     * 用户点击media_id类型按钮后，微信服务器会将开发者填写的永久素材id对应的素材下发给用户，永久素材类型可以是图片、音频、视频、图文消息。请注意：永久素材id必须是在“素材管理/新增永久素材”接口上传后获得的合法id。
     *
     * @param text
     * @param mediaID
     */
    public MenuList addMeidaMenu(String text, String mediaID) {
        menus.add(new JsonObject().put("type", "media_id").put("name", text).put("media_id", mediaID));
        return this;
    }

    /**
     * 跳转图文消息URL
     * 用户点击view_limited类型按钮后，微信客户端将打开开发者在按钮中填写的永久素材id对应的图文消息URL，永久素材类型只支持图文消息。请注意：永久素材id必须是在“素材管理/新增永久素材”接口上传后获得的合法id。
     *
     * @param text
     * @param mediaID
     */
    public MenuList addViewLimitedMenu(String text, String mediaID) {
        menus.add(new JsonObject().put("type", "view_limited").put("name", text).put("media_id", mediaID));
        return this;
    }

    /**
     * 添加微信小程序入口
     *
     * @param text     显示文本
     * @param url      网页链接 如 http://mp.weixin.qq.com
     * @param appid    小程序的appid
     * @param pagepath 小程序的页面路径 如 pages/lunar/index.html
     * @return
     */
    public MenuList addMiniProgram(String text, String url, String appid, String pagepath) {
        menus.add(new JsonObject().put("type", "miniprogram").put("name", text).put("url", url).put("appid", appid).put("pagepath", pagepath));
        return this;
    }
}
