package fzs.wechat.binder.models.message;

import fzs.wechat.shared.wechat.MessageType;
import io.vertx.core.json.JsonObject;

/**
 * Created by Hello on 2017/3/24.
 */
public class MusicMessage extends MessageAbstract {
    public MusicMessage(String touser) {
        super(touser, MessageType.music);
    }


    /* {
      "title":"MUSIC_TITLE",
      "description":"MUSIC_DESCRIPTION",
      "musicurl":"MUSIC_URL",
      "hqmusicurl":"HQ_MUSIC_URL",
      "thumb_media_id":"THUMB_MEDIA_ID"
    }*/
    @Override
    public JsonObject datas() {
        return new JsonObject()
            .put("title", title)
            .put("description", description)
            .put("musicurl", musicUrl)
            .put("hqmusicurl", hQMusicUrl)
            .put("thumb_media_id", thumbMediaId);
    }


    String title;
    String description;
    String musicUrl;
    String hQMusicUrl;
    String thumbMediaId;

    //设置音乐文件
    public MusicMessage setMusic(String title, String description, String musicUrl, String hQMusicUrl, String thumbMediaId) {
        this.title = title;
        this.description = description;
        this.musicUrl = musicUrl;
        this.hQMusicUrl = hQMusicUrl;
        this.thumbMediaId = thumbMediaId;

        return this;
    }
}
