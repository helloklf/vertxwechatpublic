package fzs.wechat.binder.models.menu;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Created by Hello on 2017/4/6.
 */
public class MenuTree extends MenuList {
    public MenuTree() {
        super();
    }

    public MenuTree(JsonObject menus) {
        super();
        if (menus == null)
            this.menus = new JsonArray();
        else
            this.menus = menus.getJsonArray("button", new JsonArray());
    }

    public MenuTree addSubButtons(String text, SubButtonMenus menuList) {
        menus.add(new JsonObject().put("name", text).put("sub_button", menuList.getMenus()));
        return this;
    }

    public JsonObject getMenus() {
        return new JsonObject().put("button", menus);
    }
}
