package fzs.wechat.binder.listener;

import fzs.wechat.binder.models.response.WechatResponse;
import io.vertx.core.Future;

/**
 * Created by Hello on 2017/3/14.
 */
public interface MessageListener {
    /**
     * 当文本消息接收
     *
     * @param fromUser       用户
     * @param messageContent 消息文字内容
     * @param answer         答复future
     */
    void onTextRecived(String fromUser, String messageContent, Future<WechatResponse> answer);

    /**
     * 当图片消息接收
     *
     * @param fromUser 用户
     * @param picUrl   图片路径
     * @param mediaID  媒体id
     * @param answer   答复future
     */
    void onImageRecived(String fromUser, String picUrl, String mediaID, Future<WechatResponse> answer);

    /**
     * 当语音消息接收
     *
     * @param fromUser    用户
     * @param format      语音格式，一般为amr，Recognition为语音识别结果，使用UTF8编码。
     * @param mediaID     语音文件媒体id
     * @param recognition 语音识别后的文字内容，如果未开启语音识别功能，将收不到此参数（需要在微信公众平台设置）
     * @param answer      答复future
     */
    void onVoiceRecived(String fromUser, String format, String mediaID, String recognition, Future<WechatResponse> answer);

    /**
     * 当视频消息接收
     *
     * @param fromUser     用户
     * @param mediaID      媒体id
     * @param thumbMediaId 缩略图id
     * @param answer       答复future
     */
    void onVideoRecived(String fromUser, String mediaID, String thumbMediaId, Future<WechatResponse> answer);

    /**
     * 当小视频消息接收
     *
     * @param fromUser     用户
     * @param mediaID      媒体id
     * @param thumbMediaId 缩略图媒体id
     * @param answer       答复future
     */
    void onShortVideoRecived(String fromUser, String mediaID, String thumbMediaId, Future<WechatResponse> answer);

    /**
     * 位置消息接收（如果在公众号中设置了获取地理位置信息，且用户允许，则在用户每次打开公众号页面时都会上报地理位置信息）
     *
     * @param fromUser  用户
     * @param latitude 维度
     * @param longitude 经度
     * @param scale     缩放等级
     * @param label     地理位置信息
     * @param answer    答复future
     */
    void onLocationRecived(String fromUser, double latitude, double longitude, int scale, String label, Future<WechatResponse> answer);

    /**
     * 当链接消息接收
     *
     * @param fromUser 用户
     * @param title    标题
     * @param url      链接
     * @param desc     说明
     * @param answer   答复future
     */
    void onLinkRecived(String fromUser, String title, String url, String desc, Future<WechatResponse> answer);
}
