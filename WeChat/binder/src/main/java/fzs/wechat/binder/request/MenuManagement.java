package fzs.wechat.binder.request;

import fzs.wechat.binder.TransceiverCenter;
import fzs.wechat.binder.models.menu.MenuTree;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;

/**
 * 菜单管理
 * Created by Hello on 2017/3/15.
 */
public class MenuManagement {
    /**
     * 获取自定义的菜单
     *
     * @param future
     */
    public void getCustomerMenus(Future<MenuTree> future) {
        Future<JsonObject> menuGetFuture = Future.<JsonObject>future().setHandler(
            r -> {
                if (r.succeeded())
                    future.complete(new MenuTree(r.result().getJsonObject("menu")));
                else
                    future.fail(r.cause());
            }
        );
        TransceiverCenter.getInstance().triggerEvent("menu.getlist", new JsonObject(), menuGetFuture);
    }

    /**
     * 设置自定义菜单
     *
     * @param menuTree
     * @param future
     */
    public void setCustomerMenus(MenuTree menuTree, Future<JsonObject> future) {
        TransceiverCenter.getInstance().triggerEvent("menu.add", menuTree.getMenus(), future);
    }

    /**
     * 删除自定义的菜单
     *
     * @param future
     */
    public void deleteCustomerMenus(Future<JsonObject> future) {
        TransceiverCenter.getInstance().triggerEvent("menu.delete", new JsonObject(), future);
    }
}
