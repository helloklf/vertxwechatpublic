package fzs.wechat.binder.models.response;

/**
 * Created by Hello on 2017/3/13.
 */
public class ImageResponse extends WechatResponse {
    public ImageResponse() {
        super(MessageType.image);
    }

    String mediaId;

    /*
    <Image>
        <MediaId><![CDATA[media_id]]></MediaId>
    </Image>
    */

    public String getMediaId() {
        return mediaId;
    }

    //设置图片ID
    public ImageResponse setImage(String mediaId) {
        this.mediaId = mediaId;

        return this;
    }
}
