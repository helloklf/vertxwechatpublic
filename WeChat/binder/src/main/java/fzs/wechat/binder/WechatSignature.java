package fzs.wechat.binder;

import fzs.wechat.shared.tools.Tools;

import java.util.UUID;

/**
 * Created by Hello on 2017/4/5.
 */
public class WechatSignature {
    public WechatSignature(String url, String jsapiTicket) {
        this.timestamp = String.valueOf(System.currentTimeMillis() / 1000);
        this.noncestr = UUID.randomUUID().toString();
        this.url = url;
        //根据微信公众号jssdk的签名算法，排除url中#符号及后面的Neri
        if (url.contains("#")) {
            this.url = url.substring(0, url.indexOf("#"));
        }
        this.jsapiTicket = jsapiTicket;
    }

    private String timestamp;
    private String url;
    private String jsapiTicket;
    private String noncestr;
    private String signature;

    /**
     * 获取单前签名的时间戳
     *
     * @return
     */
    public String getTimestamp() {
        return this.timestamp;
    }

    /**
     * 获取单前签名的随机字符串
     *
     * @return
     */
    public String getNoncestr() {
        return this.noncestr;
    }

    /**
     * 获取单前签名的签名字符串
     *
     * @return
     */
    public String getSignature() {
        if (this.signature == null) {
            try {
                this.signature = Tools.wechatSignature(jsapiTicket, noncestr, timestamp, url);
            } catch (Exception ex) {
                return null;
            }
        }
        return signature;
    }
}
