package fzs.wechat.binder.models.message;

import fzs.wechat.shared.wechat.MessageType;

/**
 * Created by Hello on 2017/3/24.
 */
public class TextMessage extends MessageAbstract {
    public TextMessage(String touser) {
        super(touser, MessageType.text);
    }

    public String content;

    public TextMessage setContent(String content) {
        this.content = content;
        return this;
    }
}