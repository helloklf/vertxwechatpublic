package fzs.wechat.binder.request;

import fzs.wechat.binder.TransceiverCenter;
import fzs.wechat.shared.tools.Tools;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.io.File;
import java.util.Date;

/**
 * 临时素材管理
 * Created by Hello on 2017/3/16.
 */
public class ResourceManagement {
    /*
        图片（image）: 1M，支持JPG格式
        语音（voice）：2M，播放长度不超过60s，支持AMR\MP3格式
        视频（video）：10MB，支持MP4格式
        缩略图（thumb）：64KB，支持JPG格式
    */

    /**
     * 上传图片（image）: 1M，支持JPG格式
     *
     * @param filePath 文件路径
     * @param future
     */
    public void uploadImage(String filePath, Future<JsonObject> future) {
        File file = new File(filePath);
        TransceiverCenter.getInstance().triggerEvent(
            "temp.image.add",
            new JsonObject().put("files", new JsonArray().add(Tools.encode(Tools.readFileToBytes(filePath)))).put("filename", file.getName()),
            future
        );
    }

    /**
     * 上传语音（voice）：2M，播放长度不超过60s，支持AMR\MP3格式
     *
     * @param filePath 文件路径
     * @param future
     */
    public void uploadVoice(String filePath, Future<JsonObject> future) {
        File file = new File(filePath);
        TransceiverCenter.getInstance().triggerEvent(
            "temp.voice.add",
            new JsonObject().put("files", new JsonArray().add(Tools.encode(Tools.readFileToBytes(filePath)))).put("filename", file.getName()),
            future
        );
    }

    /**
     * 上传视频（video）：10MB，支持MP4格式
     *
     * @param filePath 文件路径
     * @param future
     */
    public void uploadVideo(String filePath, Future<JsonObject> future) {
        /*
        *{
          "title":VIDEO_TITLE,
          "introduction":INTRODUCTION
        }
        */
        File file = new File(filePath);
        TransceiverCenter.getInstance().triggerEvent(
            "temp.video.add",
            new JsonObject()
                .put("files", new JsonArray().add(Tools.encode(Tools.readFileToBytes(filePath))))
                .put("filename", file.getName())
                .put("title", file.getName())
                .put("introduction", new Date().toString()),
            future
        );
    }

    /**
     * 上传缩略图（thumb）：64KB，支持JPG格式
     *
     * @param filePath 文件路径
     * @param future
     */
    public void uploadThumb(String filePath, Future<JsonObject> future) {
        File file = new File(filePath);
        TransceiverCenter.getInstance().triggerEvent(
            "temp.thumb.add",
            new JsonObject().put("files", new JsonArray().add(Tools.encode(Tools.readFileToBytes(filePath)))).put("filename", file.getName()),
            future
        );
    }
}
