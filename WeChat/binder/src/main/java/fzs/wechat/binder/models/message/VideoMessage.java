package fzs.wechat.binder.models.message;

import fzs.wechat.shared.wechat.MessageType;
import io.vertx.core.json.JsonObject;

/**
 * Created by Hello on 2017/3/24.
 */
public class VideoMessage extends MessageAbstract {
    public VideoMessage(String touser) {
        super(touser, MessageType.video);
    }

    /*
     "media_id":"MEDIA_ID",
      "thumb_media_id":"MEDIA_ID",
      "title":"TITLE",
      "description":"DESCRIPTION"
    */
    @Override
    public JsonObject datas() {
        return new JsonObject()
            .put("media_id", mediaId)
            .put("thumb_media_id", thumbMediaId)
            .put("title", title)
            .put("description", description);
    }

    String mediaId;
    String title;
    String description;
    String thumbMediaId;

    //设置视频文件
    public VideoMessage setVidio(String mediaID, String thumbMediaId, String title, String desc) {
        this.mediaId = mediaID;
        this.thumbMediaId = thumbMediaId;
        this.title = title;
        this.description = desc;

        return this;
    }
}
