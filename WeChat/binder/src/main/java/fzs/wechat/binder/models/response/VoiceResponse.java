package fzs.wechat.binder.models.response;

/**
 * Created by Hello on 2017/3/13.
 */
public class VoiceResponse extends WechatResponse {
    public VoiceResponse() {
        super(MessageType.voice);
    }

    /*
    <Voice>
        <MediaId><![CDATA[media_id]]></MediaId>
    </Voice>
    */
    String mediaId;

    /**
     * 获取mediaID
     *
     * @return
     */
    public String getMediaId() {
        return mediaId;
    }

    //设置语音文件
    public VoiceResponse setVoice(String mediaID) {
        this.mediaId = mediaID;

        return this;
    }
}
