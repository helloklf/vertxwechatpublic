package fzs.wechat.binder.models.response;

/**
 * Created by Hello on 2017/4/7.
 */
public class ArticlesMessageArticleItem {
    public String title;
    public String desc;
    public String picUrl;
    public String url;
}
