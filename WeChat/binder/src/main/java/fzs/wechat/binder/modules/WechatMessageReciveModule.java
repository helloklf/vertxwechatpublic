package fzs.wechat.binder.modules;

import fzs.wechat.binder.TransceiverCenter;
import fzs.wechat.binder.listener.MessageListener;
import fzs.wechat.binder.models.response.WechatResponse;
import fzs.wechat.shared.module.ModulesAbstract;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * 微信公众号消息推送处理模块（微信服务器将用户发送的消息转发到开发者服务器上）
 * Created by Hello on 2017/3/13.
 */
public class WechatMessageReciveModule implements ModulesAbstract {
    MessageListener messageListener;

    public WechatMessageReciveModule(MessageListener messageListener) {
        this.messageListener = messageListener;
    }

    @Override
    public Map<String, Handler<Message<JsonObject>>> getEvents() {
        HashMap<String, Handler<Message<JsonObject>>> registerInos = new HashMap<>();

        registerInos.put("text", this::textHandler);
        registerInos.put("image", this::imageHandler);
        registerInos.put("voice", this::voiceHandler);
        registerInos.put("video", this::videoHandler);
        registerInos.put("shortvideo", this::videoHandler);
        registerInos.put("location", this::locationHandler);
        registerInos.put("link", this::linkHandler);

        return registerInos;
    }

    public Future<WechatResponse> getAnwserFuture(Message<JsonObject> message) {
        Future<WechatResponse> responseContentFuture = Future.future();
        responseContentFuture.setHandler(ar -> {
            if (ar.succeeded()) {
                Object result = ar.result();
                JsonObject response = JsonObject.mapFrom(result);
                message.reply(response);
            } else {
                message.fail(TransceiverCenter.FAIL_ANSWER, ar.cause().getMessage());
            }
        });
        return responseContentFuture;
    }

    public void textHandler(Message<JsonObject> message) {
        JsonObject request = message.body();
        info("receive response: {0}", request);
        String fromUser = request.getString("fromUserName");
        String requestContent = request.getString("content");
        messageListener.onTextRecived(fromUser, requestContent, getAnwserFuture(message));
    }

    public void imageHandler(Message<JsonObject> message) {
        JsonObject request = message.body();
        info("receive response: {0}", request);
        String fromUser = request.getString("fromUserName");
        String picUrl = request.getString("picUrl");
        String mediaId = request.getString("mediaId");
        messageListener.onImageRecived(fromUser, picUrl, mediaId, getAnwserFuture(message));
    }

    public void voiceHandler(Message<JsonObject> message) {
        JsonObject request = message.body();
        info("receive response: {0}", request);
        String fromUser = request.getString("fromUserName");
        String format = request.getString("format");
        String mediaId = request.getString("mediaId");
        String recognition = request.getString("recognition", "");
        recognition = recognition.equals("") ? null : recognition;
        messageListener.onVoiceRecived(fromUser, format, mediaId, recognition, getAnwserFuture(message));
    }

    public void videoHandler(Message<JsonObject> message) {
        JsonObject request = message.body();
        info("receive response: {0}", request);
        String fromUser = request.getString("fromUserName");
        String thumbMediaId = request.getString("thumbMediaId");
        String mediaId = request.getString("mediaId");
        messageListener.onVideoRecived(fromUser, mediaId, thumbMediaId, getAnwserFuture(message));
    }

    public void locationHandler(Message<JsonObject> message) {
        JsonObject request = message.body();
        info("receive response: {0}", request);
        String fromUser = request.getString("fromUserName");
        double locationX = request.getDouble("location_X");
        double locationY = request.getDouble("location_Y");
        int scale = request.getInteger("scale");
        String label = request.getString("label");
        messageListener.onLocationRecived(fromUser, locationX, locationY, scale, label, getAnwserFuture(message));
    }

    public void linkHandler(Message<JsonObject> message) {
        JsonObject request = message.body();
        info("receive response: {0}", request);
        String fromUser = request.getString("fromUserName");
        String title = request.getString("title");
        String description = request.getString("description");
        String url = request.getString("url");
        messageListener.onLinkRecived(fromUser, title, description, url, getAnwserFuture(message));
    }


    /**
     * 输出调试日志
     *
     * @param message
     * @param objects
     */
    void info(Object message, Object... objects) {
        LoggerFactory.getLogger(this.getClass()).info(message, objects);
    }

    /**
     * 输出错误日志
     *
     * @param message
     * @param objects
     */
    void error(Object message, Object... objects) {
        LoggerFactory.getLogger(this.getClass()).error(message, objects);
    }
}