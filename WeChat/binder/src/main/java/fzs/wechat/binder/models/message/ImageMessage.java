package fzs.wechat.binder.models.message;

import fzs.wechat.shared.wechat.MessageType;
import io.vertx.core.json.JsonObject;

/**
 * Created by Hello on 2017/3/24.
 */
public class ImageMessage extends MessageAbstract {
    public ImageMessage(String touser) {
        super(touser, MessageType.image);
    }

    String mediaId;

    @Override
    public JsonObject datas() {
        return new JsonObject().put("media_id", mediaId);
    }

    //设置图片ID
    public ImageMessage setImage(String mediaId) {
        this.mediaId = mediaId;

        return this;
    }
}
