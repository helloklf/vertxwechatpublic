package fzs.wechat.binder.listener;


import io.vertx.core.Future;

/**
 * Created by Hello on 2017/3/14.
 * <xml>
 * <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[FromUser]]></FromUserName>
 * <CreateTime>123456789</CreateTime>
 * <MsgType><![CDATA[event]]></MsgType>
 * <Event><![CDATA[subscribe]]></Event>
 * </xml>
 */
public interface EventListener {
    /**
     * 用户关注微信公众号
     *
     * @param fromUser
     * @param answer
     */
    void onSubscribe(String fromUser, Future answer);

    /**
     * 用户通过扫描场景二维码关注微信公众号
     *
     * @param fromUser
     * @param enterKey
     * @param ticket
     */
    void onQrScanAfterSubscribe(String fromUser, String enterKey, String ticket);

    /**
     * 用户取消关注微信公众号
     *
     * @param fromUser
     */
    void onUnSubscribe(String fromUser);

    /**
     * 用户扫描二维码
     *
     * @param fromUser
     * @param enterKey
     * @param ticket
     */
    void onQrScan(String fromUser, String enterKey, String ticket);

    /**
     * 获取了用户的位置信息
     *
     * @param fromUser
     * @param latitude
     * @param longitude
     * @param precision
     */
    void onLocation(String fromUser, Double latitude, Double longitude, Double precision);

    /**
     * 用户点击了菜单项
     *
     * @param fromUser
     * @param eventKey
     * @param answer
     */
    void onMenuClick(String fromUser, String eventKey, Future answer);

    /**
     * 用户点击了菜单项且打开了链接
     *
     * @param fromUser
     * @param eventKey
     */
    void onMenuOpenView(String fromUser, String eventKey);
}