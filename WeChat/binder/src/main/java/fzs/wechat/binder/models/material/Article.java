package fzs.wechat.binder.models.material;

import io.vertx.core.json.JsonObject;

/**
 * Created by Hello on 2017/4/7.
 */
public class Article {
    /*{
     "title": TITLE, //标题
     "thumb_media_id": THUMB_MEDIA_ID, //封面图片id，必须是永久素材
     "author": AUTHOR, //作者
     "digest": DIGEST,      //摘要
     "show_cover_pic": SHOW_COVER_PIC(0 / 1),   //是否显示封面，1/0
     "content": CONTENT,    //内容
     "content_source_url": CONTENT_SOURCE_URL //原文地址
     }
     */
    private String title,thumbMediaId,author,digest,showCoverPic,content,contentSourceUrl;

    public Article (String title,String thumbMediaId,String author,String digest,String showCoverPic,String content,String contentSourceUrl){
        this.title = title;
        this.thumbMediaId = title;
        this.author = title;
        this.digest = title;
        this.showCoverPic = title;
        this.content = title;
        this.contentSourceUrl = title;
    }

    public JsonObject datas() {
        return new JsonObject()
            .put("title", title)
            .put("thumb_media_id", thumbMediaId)
            .put("author", author)
            .put("digest", digest)
            .put("show_cover_pic", showCoverPic)
            .put("content", content)
            .put("content_source_url", contentSourceUrl);
    }
}
