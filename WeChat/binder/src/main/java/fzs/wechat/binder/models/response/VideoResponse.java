package fzs.wechat.binder.models.response;

/**
 * Created by Hello on 2017/3/13.
 */
public class VideoResponse extends WechatResponse {
    public VideoResponse() {
        super(MessageType.video);
    }

    /*
    <Video>
        <MediaId><![CDATA[media_id]]></MediaId>
        <Title><![CDATA[title]]></Title>
        <Description><![CDATA[description]]></Description>
    </Video>
    */

    String mediaId;
    String title;
    String description;

    /**
     * 设置视频文件
     *
     * @param mediaID 视频文件mediaid，注意：只能使用永久素材视频
     * @param title   视频标题
     * @param desc    视频说明
     * @return
     */
    public VideoResponse setVidio(String mediaID, String title, String desc) {
        this.mediaId = mediaID;
        this.title = title;
        this.description = desc;

        return this;
    }

    public String getMediaId() {
        return mediaId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
