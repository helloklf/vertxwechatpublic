package fzs.wechat.binder.models.response;

import java.util.ArrayList;

/**
 * Created by Hello on 2017/3/13.
 */
public class ArticleResponse extends WechatResponse {
    public ArticleResponse() {
        super(MessageType.news);
    }
    /*
    <ArticleCount>2</ArticleCount>
    <Articles>
        <item>
            <Title><![CDATA[title1]]></Title>
            <Description><![CDATA[description1]]></Description>
            <PicUrl><![CDATA[picurl]]></PicUrl>
            <Url><![CDATA[url]]></Url>
        </item>
        <item>
            <Title><![CDATA[title]]></Title>
            <Description><![CDATA[description]]></Description>
            <PicUrl><![CDATA[picurl]]></PicUrl>
            <Url><![CDATA[url]]></Url>
        </item>
    </Articles>
    */

    ArrayList<ArticlesMessageArticleItem> articles = new ArrayList<ArticlesMessageArticleItem>();

    //添加文章
    public ArticleResponse addArticle(String title, String desc, String picUrl, String url) {
        ArticlesMessageArticleItem article = new ArticlesMessageArticleItem();
        article.title = title;
        article.desc = desc;
        article.picUrl = picUrl;
        article.url = url;

        articles.add(article);

        return this;
    }

    public ArrayList<ArticlesMessageArticleItem> getArticles() {
        return articles;
    }
}
