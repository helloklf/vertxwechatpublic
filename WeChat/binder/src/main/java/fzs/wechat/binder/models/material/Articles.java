package fzs.wechat.binder.models.material;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;

/**
 * Created by Hello on 2017/4/7.
 */
public class Articles {
    /**
     * {
     * "articles": [{
     * "title": TITLE, //标题
     * "thumb_media_id": THUMB_MEDIA_ID, //封面图片id，必须是永久素材
     * "author": AUTHOR, //作者
     * "digest": DIGEST,      //摘要
     * "show_cover_pic": SHOW_COVER_PIC(0 / 1),   //是否显示封面，1/0
     * "content": CONTENT,    //内容
     * "content_source_url": CONTENT_SOURCE_URL //原文地址
     * },
     * //若新增的是多图文素材，则此处应有几段articles结构，最多8段
     * ]
     * }
     */

    ArrayList<Article> articles = new ArrayList<>();

    public Articles() {

    }

    public Articles addArticles(Article article) {
        this.articles.add(article);

        return this;
    }

    public JsonObject datas() {
        JsonArray jsonArray = new JsonArray();
        for (int i = 0; i < articles.size(); i++) {
            jsonArray.add(articles.get(i).datas());
        }

        return new JsonObject().put("articles", jsonArray);
    }
}
