package fzs.wechat.binder.request;

import fzs.wechat.binder.TransceiverCenter;
import fzs.wechat.binder.models.message.MessageAbstract;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;

/**
 * 消息发送
 * Created by Hello on 2017/3/24.
 */
public class MessageSender {
    /**
     * 发送消息
     *
     * @param message 消息体
     * @param <T>     消息类型
     */
    public <T extends MessageAbstract> void sendMessage(T message, Future<JsonObject> future) {
        JsonObject form = message.toJsonObject();
        TransceiverCenter.getInstance().triggerEvent("customservice.sendmsg", form, future);
        System.out.print(form);
    }

    /**
     * 发送消息，以指定客服的名义
     *
     * @param message       消息体
     * @param customService 客服id
     * @param <T>           消息类型
     */
    public <T extends MessageAbstract> void sendMessage(T message, String customService, Future<JsonObject> future) {
        message.setCustomservice(customService);

        JsonObject form = message.toJsonObject();
        TransceiverCenter.getInstance().triggerEvent("customservice.sendmsg", form, future);
        System.out.print(form);
    }
}
