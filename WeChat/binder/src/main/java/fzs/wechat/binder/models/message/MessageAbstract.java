package fzs.wechat.binder.models.message;

import fzs.wechat.shared.wechat.MessageType;
import io.vertx.core.json.JsonObject;

/**
 * Created by Hello on 2017/3/24.
 */
public class MessageAbstract {
    public MessageAbstract(String touser, MessageType msgtype) {
        this.touser = touser;
        this.msgtype = msgtype.name().toLowerCase();
    }

    String touser;
    String msgtype;
    String customservice;

    /**
     * 设置客服账号：可选
     *
     * @param customserviceAccount
     * @return
     */
    public void setCustomservice(String customserviceAccount) {
        this.customservice = customserviceAccount;
    }

    public JsonObject datas() {
        return JsonObject.mapFrom(this);
    }

    public JsonObject toJsonObject() {
        JsonObject datas = datas();

        JsonObject message = new JsonObject()
            .put("touser", touser)
            .put("msgtype", msgtype)
            .put(msgtype, datas);

        if (customservice != null) {
            message.put("customservice", new JsonObject().put("kf_account", customservice));
        }

        return message;
    }
}