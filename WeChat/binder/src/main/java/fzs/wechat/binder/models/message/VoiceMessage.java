package fzs.wechat.binder.models.message;

import fzs.wechat.shared.wechat.MessageType;
import io.vertx.core.json.JsonObject;

/**
 * Created by Hello on 2017/3/24.
 */
public class VoiceMessage extends MessageAbstract {
    public VoiceMessage(String touser) {
        super(touser, MessageType.voice);
    }

    String mediaId;


    @Override
    public JsonObject datas() {
        return new JsonObject().put("media_id", mediaId);
    }

    //设置语音文件
    public VoiceMessage setVoice(String mediaID) {
        this.mediaId = mediaID;

        return this;
    }
}
