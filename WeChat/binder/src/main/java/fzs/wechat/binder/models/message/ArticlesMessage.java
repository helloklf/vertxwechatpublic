package fzs.wechat.binder.models.message;

import fzs.wechat.shared.wechat.MessageType;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;

/**
 * Created by Hello on 2017/3/24.
 */
public class ArticlesMessage extends MessageAbstract {
    public ArticlesMessage(String touser) {
        super(touser, MessageType.news);
    }

    ArrayList<ArticlesMessageArticleItem> articles = new ArrayList<ArticlesMessageArticleItem>();

    /* "news":{
        "articles": [
         {
             "title":"Happy Day",
             "description":"Is Really A Happy Day",
             "url":"URL",
             "picurl":"PIC_URL"
         },
         {
             "title":"Happy Day",
             "description":"Is Really A Happy Day",
             "url":"URL",
             "picurl":"PIC_URL"
         }
         ]
    }*/

    @Override
    public JsonObject datas() {
        JsonArray articles = new JsonArray();
        this.articles.forEach(item -> {
            articles.add(JsonObject.mapFrom(item));
        });
        return new JsonObject().put("articles", articles);
    }

    //添加文章
    public ArticlesMessage addArticle(String title, String desc, String picUrl, String url) {
        ArticlesMessageArticleItem article = new ArticlesMessageArticleItem();
        article.title = title;
        article.description = desc;
        article.picurl = picUrl;
        article.url = url;

        articles.add(article);

        return this;
    }
}
