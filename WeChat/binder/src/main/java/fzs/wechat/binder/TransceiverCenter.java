package fzs.wechat.binder;

import fzs.wechat.shared.EventBusHelper;
import fzs.wechat.shared.module.ModulesManager;
import fzs.wechat.shared.wechat.Consts;
import fzs.wechat.shared.wechat.models.ServiceConfig;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 收发中心
 * Created by WEI on 2017/3/9.
 */
public class TransceiverCenter {
    public static final int FAIL_ANSWER = 1;
    private static TransceiverCenter instance;
    private EventBus eventBus;
    private String eventPrefix = null;
    private ModulesManager modulesManager;
    private EventBusHelper eventBusHelper;
    private String culusteID;
    private DeliveryOptions defaultDeliveryOptions = new DeliveryOptions() {{
        setSendTimeout(15000);
    }};

    private TransceiverCenter() {
    }

    public static void deployAsCluster(final ServiceConfig wechatConfig, Future<Vertx> deployFuture) {
        Logger.getLogger("com.hazelcast").setLevel(Level.WARNING);

        ClusterManager mgr = new HazelcastClusterManager();
        VertxOptions options = new VertxOptions().setClusterManager(mgr);

        Vertx.clusteredVertx(options, ar -> {
            if (ar.succeeded()) {
                EventBus eventBus = ar.result().eventBus();
                Future initFuture = Future.future();
                initFuture.setHandler(initHandler -> {
                    if (initFuture.isComplete())
                        deployFuture.complete(ar.result());
                    else
                        deployFuture.fail(initFuture.cause());
                });
                TransceiverCenter.getInstance().init(wechatConfig, eventBus, mgr.getNodeID(), initFuture);
            } else {
                LoggerFactory.getLogger(TransceiverCenter.class).error("部署集群遇到问题，所以程序未能成功启动");
                deployFuture.fail(ar.cause());
            }
        });
    }

    public synchronized static TransceiverCenter getInstance() {
        if (instance == null)
            instance = new TransceiverCenter();
        return instance;
    }

    public synchronized ModulesManager getModulesManager() {
        if (modulesManager == null)
            modulesManager = new ModulesManager(eventPrefix, eventBus);

        return modulesManager;
    }

    /**
     * 初始化
     *
     * @param serviceConfig
     * @param eventBus
     * @param culusteID
     * @param initFuture
     */
    private void init(final ServiceConfig serviceConfig, EventBus eventBus, String culusteID, Future initFuture) {
        this.eventBus = eventBus;
        this.eventPrefix = EventBusHelper.getEventPrefix(serviceConfig);
        this.eventBusHelper = new EventBusHelper(eventBus, eventPrefix);
        this.culusteID = culusteID;

        DeliveryOptions deliveryOptions = new DeliveryOptions();

        //设置微信配置
        eventBusHelper.triggerEvent(
            Consts.INIT_EVENT_ADDRESS,
            new JsonObject()
                .put("host", serviceConfig.getMicroServiceConfig().host)
                .put("cluster", culusteID),
            deliveryOptions,
            r -> {
                if (r.succeeded()) {
                    initFuture.complete();
                } else {
                    error(r.cause());
                    initFuture.fail(r.cause());
                }
            });
    }

    /**
     * 销毁
     *
     * @param eventBus
     * @param future
     */
    public void destroy(ServiceConfig serviceConfig, EventBus eventBus, Future future) {
        eventBus.send(Consts.UNINIT_EVENT_ADDRESS,
            new JsonObject()
                .put("host", serviceConfig.getMicroServiceConfig().host)
                .put("cluster", this.culusteID),
            r -> {
                if (r.succeeded())
                    future.complete();
                else
                    future.fail(r.cause());
            });
    }

    /**
     * 触发事件：点对点的事件，最多只有一个事件处理程序接收到此事件，且必须要做出回复，否则认为调用失败
     *
     * @param eventName  事件
     * @param jsonObject 数据
     * @param future     回调
     */
    public void triggerEvent(String eventName, JsonObject jsonObject, Future<JsonObject> future) {
        triggerEvent(eventName, jsonObject, this.defaultDeliveryOptions, future);
    }

    /**
     * 触发事件：点对点的事件，最多只有一个事件处理程序接收到此事件，且必须要做出回复，否则认为调用失败
     *
     * @param eventName       事件
     * @param jsonObject      数据
     * @param deliveryOptions 传递设置
     * @param future          回调
     */
    public void triggerEvent(String eventName, JsonObject jsonObject, DeliveryOptions deliveryOptions, Future<JsonObject> future) {
        eventBusHelper.triggerEvent(eventName, jsonObject, deliveryOptions, handler -> {
            try {
                if (!handler.succeeded()) {
                    if (future != null)
                        future.fail(handler.cause());
                    error("发送请求失败：" + eventName);
                } else if (future != null)
                    future.complete(handler.result().body());
            } catch (Exception ex) {
                if (future != null)
                    future.fail(ex.getCause());
                error("发送请求失败：" + eventName);
            }
        });
    }

    /**
     * 发布事件：广播形式的事件，抛出事件和数据内容而不关心响应情况
     *
     * @param eventName  事件
     * @param jsonObject 数据
     */
    public void pulishEvent(String eventName, JsonObject jsonObject) {
        eventBusHelper.pulishEvent(eventName, jsonObject == null ? new JsonObject() : jsonObject);
    }

    /**
     * 创建JSSSDK签名
     *
     * @param url              使用签名的页面
     * @param signatureHandler 签名结果回调
     */
    public void createJSSDKSignature(String url, Handler<WechatSignature> signatureHandler) {
        eventBusHelper.triggerEvent(Consts.MICRO_SERVICE_GETJSTICKET_EVENT, new JsonObject(), handler -> {
            if (!handler.succeeded()) {
                error("发送请求失败：%s", Consts.MICRO_SERVICE_GETJSTICKET_EVENT);
                signatureHandler.handle(null);
            }

            JsonObject result = ((JsonObject) handler.result().body());
            String jsticket = result.getString("jsticket");
            WechatSignature wechatSignature = new WechatSignature(url, jsticket);
            signatureHandler.handle(wechatSignature);
        });
    }

    /**
     * 输出调试日志
     *
     * @param message
     * @param objects
     */
    void info(Object message, Object... objects) {
        LoggerFactory.getLogger(this.getClass()).info(message, objects);
    }

    /**
     * 输出错误日志
     *
     * @param message
     * @param objects
     */
    void error(Object message, Object... objects) {
        LoggerFactory.getLogger(this.getClass()).error(message, objects);
    }
}
