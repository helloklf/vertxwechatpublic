package fzs.wechat.binder.modules;

import fzs.wechat.binder.TransceiverCenter;
import fzs.wechat.binder.listener.EventListener;
import fzs.wechat.binder.models.response.WechatResponse;
import fzs.wechat.shared.module.ModulesAbstract;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * 微信公众号事件推送处理模块（微信服务器将用户操作事件发到开发者服务器上）
 * Created by Hello on 2017/3/13.
 */
public class WechatEventReciveModule implements ModulesAbstract {
    EventListener eventListener;

    public WechatEventReciveModule(EventListener eventListener) {
        this.eventListener = eventListener;
    }

    @Override
    public Map<String, Handler<Message<JsonObject>>> getEvents() {
        HashMap<String, Handler<Message<JsonObject>>> registerInos = new HashMap<>();

        registerInos.put("event.click", this::menuHandler);
        registerInos.put("event.view", this::menuOpenViewHandler);
        registerInos.put("event.subscribe", this::subscribeHandler);
        registerInos.put("event.unsubscribe", this::unSubscribeHandler);
        registerInos.put("event.location", this::locationHandler);
        registerInos.put("event.scan", this::qrScanHandler);

        return registerInos;
    }

    public Future<WechatResponse> getAnwserFuture(Message<JsonObject> message) {
        Future<WechatResponse> responseContentFuture = Future.future();
        responseContentFuture.setHandler(ar -> {
            if (ar.succeeded()) {
                Object result = ar.result();
                JsonObject response = JsonObject.mapFrom(result);
                message.reply(response);
            } else {
                message.fail(TransceiverCenter.FAIL_ANSWER, ar.cause().getMessage());
            }
        });
        return responseContentFuture;
    }

    public void menuHandler(Message<JsonObject> message) {
        JsonObject request = message.body();
        info("receive response: {0}", request);
        String fromUser = request.getString("fromUserName");
        eventListener.onMenuClick(
            fromUser, request.getString("eventKey"), getAnwserFuture(message));
    }

    public void menuOpenViewHandler(Message<JsonObject> message) {
        JsonObject request = message.body();
        info("receive response: {0}", request);
        message.reply("");
        eventListener.onMenuOpenView(request.getString("fromUserName"), request.getString("eventKey"));
    }

    /**
     * 对于用户订阅（关注），有两种方式，一种是用直接订阅，另一种是通过扫描场景二维码订阅
     * @param message
     */
    public void subscribeHandler(Message<JsonObject> message) {
        JsonObject request = message.body();
        info("receive response: {0}", request);
        String fromUser = request.getString("fromUserName");
        String ticket = request.getString("ticket", null);
        if (ticket == null || ticket.equals("")) {
            Future<WechatResponse> responseContentFuture = getAnwserFuture(message);
            eventListener.onSubscribe(fromUser, responseContentFuture);
        } else {
            eventListener.onQrScanAfterSubscribe(fromUser, request.getString("Ticket", null), ticket);
            message.reply("");
        }
    }

    public void unSubscribeHandler(Message<JsonObject> message) {
        JsonObject request = message.body();
        info("receive response: {0}", request);
        eventListener.onUnSubscribe(request.getString("fromUserName"));
        message.reply("");
    }

    public void qrScanHandler(Message<JsonObject> message) {
        JsonObject request = message.body();
        info("receive response: {0}", request);
        String fromUser = request.getString("fromUserName");
        String eventKey = request.getString("eventKey");
        String ticket = request.getString("ticket");
        eventListener.onQrScan(fromUser, eventKey, ticket);
    }

    public void locationHandler(Message<JsonObject> message) {
        JsonObject request = message.body();
        info("receive response: {0}", request);
        String fromUser = request.getString("fromUserName");
        Double latitude = request.getDouble("latitude");
        Double longitude = request.getDouble("longitude");
        Double precision = request.getDouble("precision");
        eventListener.onLocation(fromUser, latitude, longitude, precision);
    }


    /**
     * 输出调试日志
     *
     * @param message
     * @param objects
     */
    void info(Object message, Object... objects) {
        LoggerFactory.getLogger(this.getClass()).info(message, objects);
    }

    /**
     * 输出错误日志
     *
     * @param message
     * @param objects
     */
    void error(Object message, Object... objects) {
        LoggerFactory.getLogger(this.getClass()).error(message, objects);
    }
}
