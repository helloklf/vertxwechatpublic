package fzs.wechat.binder.models.response;

/**
 * Created by Hello on 2017/3/13.
 */
public class MusicResponse extends WechatResponse {
    public MusicResponse() {
        super(MessageType.music);
    }
    /*
    <Music>
        <Title><![CDATA[TITLE]]></Title>
        <Description><![CDATA[DESCRIPTION]]></Description>
        <MusicUrl><![CDATA[MUSIC_Url]]></MusicUrl>
        <HQMusicUrl><![CDATA[HQ_MUSIC_Url]]></HQMusicUrl>
        <ThumbMediaId><![CDATA[media_id]]></ThumbMediaId>
    </Music>
    */

    String title;
    String description;
    String musicUrl;
    String hQMusicUrl;
    String thumbMediaId;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getMusicUrl() {
        return musicUrl;
    }

    public String gethQMusicUrl() {
        return hQMusicUrl;
    }

    public String getThumbMediaId() {
        return thumbMediaId;
    }

    //设置音乐文件
    public MusicResponse setMusic(String title, String description, String musicUrl, String hQMusicUrl, String thumbMediaId) {
        this.title = title;
        this.description = description;
        this.musicUrl = musicUrl;
        this.hQMusicUrl = hQMusicUrl;
        this.thumbMediaId = thumbMediaId;

        return this;
    }
}
