package fzs.wechat.binder.request;

import fzs.wechat.binder.TransceiverCenter;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;

/**
 * TODO:客服管理 由于测试号一直提示未开启自定义客服功能，因此暂时未完成此模块测试
 * Created by Hello on 2017/3/16.
 */
//请注意，必须先在公众平台官网为公众号设置微信号后才能使用该能力。
public class CustomerServiceManagement {
    /* INFO:发送客服消息的功能接口在 MessageSender 中实现 */

    public void getCustomerServiceAccounts(Future<JsonObject> future) {
        TransceiverCenter.getInstance().triggerEvent("customservice.getlist", null, future);
    }
}
