package fzs.wechat.binder.models.response;

/**
 * Created by WEI on 2017/3/9.
 */
public abstract class WechatResponse {
    String msgType;

    public WechatResponse(MessageType msgType) {
        setMsgType(msgType);
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(MessageType msgType) {
        this.msgType = msgType.name();
    }

    public enum MessageType {
        text,
        music,
        image,
        voice,
        video,
        news
    }
}
